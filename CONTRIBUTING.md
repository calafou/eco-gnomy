# Contributing

## General recommendations:

* Try to prepare self-contained MR 
* Run tests locally before pushing
* Have patience with reviews
* Keep up to date your master


