from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal as D

from django.db import models
from django.db.models import Q
from django.db.models import Sum
from django.db.models.functions import Coalesce

from druids.calculators import ProjectCalculator


class ProyectoProductivo(models.Model):
    """
    El modelo que recoge la información sobre un proyecto productivo.
    """
    nombre = models.CharField(max_length=256)
    cuota_espacio = models.DecimalField(max_digits=5, decimal_places=2)
    fecha_creacion = models.DateField(verbose_name="Fecha de creación del proyecto")
    fecha_disolucion = models.DateField(verbose_name="Fecha de disolución del proyecto",
                                        null=True,
                                        blank=True)
    comentarios = models.TextField(blank=True, max_length=2048, verbose_name="Comentarios AG de Economía")

    @property
    def is_active(self):
        return self.fecha_disolucion is None

    def _get_aportacion_aggregation(self, queryset):
        return queryset.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]

    @property
    def aportacion_beneficios_sin_historico(self):
        return self._get_aportacion_aggregation(
            self.entradas_beneficio.filter(
                Q(
                    ~Q(registro__nombre="Caja histórico"),
                ),
            )
        )
    @property
    def aportacion_beneficios(self):
        return self._get_aportacion_aggregation(
            self.entradas_beneficio
        )

    @property
    def aportacion_alquiler_sin_historico(self):
        return self._get_aportacion_aggregation(
            self.entradas_alquiler.filter(
                Q(
                    ~Q(registro__nombre="Caja histórico"),
                ),
            )
        )

    @property
    def aportacion_alquiler(self):
        return self.get_aportacion_aggregation(self.entradas_espacio.aggregate)

    @property
    def deuda_espacio(self):
        return ProjectCalculator().project_space_debt(
            self.entradas_espacio.values("importe"),
            self.cuota_espacio,
            self.fecha_creacion,
            self.fecha_disolucion,
        )

    @property
    def ultimas_aportaciones_espacio(self):
        return self.entradas_espacio.all().order_by("-fecha")[:20]

    @property
    def ultimas_aportaciones_beneficio(self):
        return self.entradas_beneficio.all().order_by("-fecha")[:20]

    def __str__(self):
        return self.nombre
