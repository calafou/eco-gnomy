from decimal import Decimal as D

from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce


class Registro(models.Model):
    """
    Clase genérica para los difertes registros contables:
    """
    nombre = models.CharField(max_length=32)

    @property
    def saldo(self):
        return self.entrada_set.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"] - self.salida_set.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]

    def __str__(self):
        return self.nombre


class Metalico(Registro):
    """
    El modelo que recoge la información del registro de
    movimientos en metálico.
    """
    class Meta:
        verbose_name = "Metálico"


class Banco(Registro):
    """
    El modelo que recoge la información del registro de
    un banco.
    """
    class Meta:
        verbose_name = "Banco"


class PayPal(Registro):
    """
    El modelo que recoge la información del registro de
    una cuenta de PayPal.
    """
    class Meta:
        verbose_name = "PayPal"
