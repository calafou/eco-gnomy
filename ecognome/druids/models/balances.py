from decimal import Decimal as D

from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce

from .entradas import Entrada
from .salidas import Salida


class Balance(models.Model):
    """
    El modelo que recoge la información de un balance.
    """
    nombre = models.CharField(max_length=256)
    descripcion = models.CharField(max_length=512, verbose_name="Descripción")
    fecha_de_inicio = models.DateField()
    fecha_de_final = models.DateField()

    @property
    def entradas(self):
        return Entrada.objects.filter(
            fecha__range=[self.fecha_de_inicio, self.fecha_de_final]
        )

    @property
    def salidas(self):
        return Salida.objects.filter(
            fecha__range=[self.fecha_de_inicio, self.fecha_de_final]
        )

    @property
    def subtotal(self):
        return self.entradas.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00"))
        )["subtotal"] - self.salidas.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00"))
        )["subtotal"]

    def __str__(self):
        return ("%s: %s - %s" % (
            self.nombre, self.descripcion, str(self.subtotal))
        )
