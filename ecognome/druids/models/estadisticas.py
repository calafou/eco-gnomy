from decimal import Decimal as D

from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce

from .entradas import Entrada
from .salidas import Salida


class ReporteMensualPorTipo(models.Model):
    """
    El modelo que recoge un reporte mensual
    de entradas y salidas por tipo
    """
    fecha_de_inicio = models.DateField()
    fecha_de_final = models.DateField()

    def _get_subtotal_for_model_and_kind(self, model, kind):
        return model.objects.filter(
            tipo=kind, fecha__range=[self.fecha_de_inicio, self.fecha_de_final],
        ).aggregate(subtotal=Coalesce(Sum("importe"), D("0.00")))["subtotal"]

    @property
    def subtotal_entradas_mantenimiento(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "mantenimiento")

    @property
    def subtotal_entradas_piso(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "piso")

    @property
    def subtotal_entradas_alquiler(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "alquiler")

    @property
    def subtotal_entradas_donativo(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "donativo")

    @property
    def subtotal_entradas_evento(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "evento")

    @property
    def subtotal_entradas_espacio_pp(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "espacio_pp")

    @property
    def subtotal_entradas_beneficio_pp(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "beneficio_pp")

    @property
    def subtotal_entradas_trastero(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "trastero")

    @property
    def subtotal_entradas_otra(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "otra")

    @property
    def subtotal_entradas_transferencia(self):
        return self._get_subtotal_for_model_and_kind(Entrada, "transferencia")

    @property
    def subtotal_salidas_alquiler_compra(self):
        return self._get_subtotal_for_model_and_kind(Salida, "alquiler_compra")

    @property
    def subtotal_salidas_material_rehabilitacion(self):
        return self._get_subtotal_for_model_and_kind(
            Salida, "material_rehabilitacion"
        )

    @property
    def subtotal_salidas_butano(self):
        return self._get_subtotal_for_model_and_kind(Salida, "butano")

    @property
    def subtotal_salidas_transferencia(self):
        return self._get_subtotal_for_model_and_kind(Salida, "tranferencia")

    @property
    def subtotal_salidas_impuesto(self):
        return self._get_subtotal_for_model_and_kind(Salida, "impuesto")

    @property
    def subtotal_salidas_comision(self):
        return self._get_subtotal_for_model_and_kind(Salida, "comision")

    @property
    def subtotal_salidas_internet(self):
        return self._get_subtotal_for_model_and_kind(Salida, "internet")

    @property
    def subtotal_salidas_agua(self):
        return self._get_subtotal_for_model_and_kind(Salida, "agua")

    @property
    def subtotal_salidas_fiare(self):
        return self._get_subtotal_for_model_and_kind(Salida, "fiare")

    @property
    def subtotal_salidas_otra(self):
        return self._get_subtotal_for_model_and_kind(Salida, "otra")

    @property
    def subtotal_entradas(self):
        return sum([
            self.subtotal_entradas_mantenimiento,
            self.subtotal_entradas_piso,
            self.subtotal_entradas_alquiler,
            self.subtotal_entradas_donativo,
            self.subtotal_entradas_evento,
            self.subtotal_entradas_espacio_pp,
            self.subtotal_entradas_beneficio_pp,
            self.subtotal_entradas_trastero,
            self.subtotal_entradas_otra,
            self.subtotal_entradas_transferencia,
        ])

    @property
    def subtotal_salidas(self):
        return sum([
            self.subtotal_salidas_alquiler_compra,
            self.subtotal_salidas_material_rehabilitacion,
            self.subtotal_salidas_butano,
            self.subtotal_salidas_transferencia,
            self.subtotal_salidas_impuesto,
            self.subtotal_salidas_comision,
            self.subtotal_salidas_internet,
            self.subtotal_salidas_agua,
            self.subtotal_salidas_fiare,
            self.subtotal_salidas_otra,
        ])

    @property
    def subtotal(self):
        return self.subtotal_entradas - self.subtotal_salidas
    

    def __str__(self):
        return ("%s - %s: %s" % (
            self.fecha_de_inicio, self.fecha_de_final, str(self.subtotal))
        )

