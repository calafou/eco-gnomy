from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.exceptions import NON_FIELD_ERRORS

from .registros import Registro


class Salida(models.Model):
    """
    El modelo que recoge la información de una salida.
    """
    CUOTA_ALQUILER_FINCA = "alquiler_compra"
    MATERIAL_REHABILITACION = "material_rehabilitacion"
    BUTANO = "butano"
    TRANSFERENCIA = "tranferencia"
    IMPUESTO = "impuesto"
    COMISION = "comision"
    INTERNET = "internet"
    AGUA = "agua"
    FIARE = "fiare"
    OTRA = "otra"

    TIPOS_SALIDA = (
        (CUOTA_ALQUILER_FINCA, "Cuota de alquiler de la finca"),
        (MATERIAL_REHABILITACION, "Material de rehabilitación"),
        (BUTANO, "Bombonas de butano"),
        (TRANSFERENCIA, "Transferencia a una cuenta nuestra"),
        (IMPUESTO, "Impuesto"),
        (COMISION, "Comisión"),
        (INTERNET, "Internet"),
        (AGUA, "Agua"),
        (FIARE, "Fiare"),
        (OTRA, "Otra Salida"),
        )
    
    fecha = models.DateField()
    importe = models.DecimalField(max_digits=7, decimal_places=2)
    tipo_salida = models.CharField(max_length=128, choices=TIPOS_SALIDA, verbose_name="Tipo de Salida")
    comentarios = models.CharField(max_length=512, blank=True, null=True)
    usuaria_economia = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="Usuaria de economía")
    registro = models.ForeignKey(Registro, on_delete=models.PROTECT)

    def validate_unique(self, *args, **kwargs):
        super(Salida, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             tipo_salida=self.tipo_salida,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )

    def __str__(self):
        return self.tipo_salida
