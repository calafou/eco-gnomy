from django.db import models


class CuotaMantenimiento(models.Model):
    """
    El modelo que recoge la información sobre una cuota de mantenimiento.
    """
    HABITANTE = "habitante"
    INVITADO = "invitado"
    HUESPED = "huesped"
    TIPOS_CUOTA = (
        (HABITANTE, "Habitante"),
        (INVITADO, "Invitado"),
        (HUESPED, "Huesped"),
        )
    
    importe = models.DecimalField(max_digits=4, decimal_places=2)
    tipo = models.CharField(max_length=12, choices=TIPOS_CUOTA)
    descripcion = models.CharField(max_length=512, verbose_name="Descripción")
    fecha_activacion = models.DateField()
    fecha_desactivacion = models.DateField(blank=True, null=True)
    
    def __str__(self):
        return ("%s - %s€ - %s" % (self.tipo, str(self.importe), self.descripcion))
    

class CuotaPiso(models.Model):
    """
    El modelo que recoge la información sobre una cuota de adquisición
    del derecho de uso permanente de una vivienda.
    """
    importe = models.DecimalField(max_digits=5, decimal_places=2)
    descripcion = models.CharField(max_length=512, verbose_name="Descripción")
    cuota_entrada = models.DecimalField(max_digits=5, decimal_places=2)
    cuota_onduline = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return ("%s€ - %s" % (str(self.importe), self.descripcion))
