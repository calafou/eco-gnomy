from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.exceptions import NON_FIELD_ERRORS

from .registros import Registro, Banco
from .personas import Persona
from .modelosmixtos import HabitantePiso, PersonaTrastero
from .proyectos import ProyectoProductivo


class Entrada(models.Model):
    """
    Clase genérica para una entrada en un Registro
    """
    MANTENIMIENTO = "mantenimiento"
    PISO = "piso"
    ALQUILER = "alquiler"
    DONATIVO = "donativo"
    EVENTO = "evento"
    ESPACIO_PP = "espacio_pp"
    BENEFICIO_PP = "beneficio_pp"
    TRASTERO = "trastero"
    OTRA = "otra"
    TRANSFERENCIA = "transferencia"
    
    TIPOS_ENTRADA = (
        (MANTENIMIENTO, "Mantenimiento"),
        (PISO, "Cuota de Piso"),
        (ALQUILER, "Alquiler"),
        (DONATIVO, "Donativo"),
        (EVENTO, "Beneficios Evento"),
        (ESPACIO_PP, "Alquiler Espacio Proy. Prod."),
        (BENEFICIO_PP, "Beneficio Proy. Prod."),
        (TRASTERO, "Alquiler de Trastero"),
        (OTRA, "Otra Entrada"),
        (TRANSFERENCIA, "Transferencia"),
    )
    
    fecha = models.DateField()
    importe = models.DecimalField(max_digits=7, decimal_places=2)
    comentarios = models.CharField(max_length=512, blank=True, null=True)
    usuaria_economia = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="Usuaria de economía")
    registro = models.ForeignKey(Registro, on_delete=models.PROTECT)
    tipo = models.CharField(max_length=64, choices=TIPOS_ENTRADA)

    def __str__(self):
        return "%s - %s - %s - %s" % (self.fecha, self.tipo, self.importe, self.registro)


class EntradaMantenimiento(Entrada):
    """
    El modelo que recoge la información de una entrada
    de mantenimiento.
    Las aportadoras de una cuota de mantenimiento son
    cualquiera subclase de Persona.
    """
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT, related_name="entradas_mantenimiento")

    def validate_unique(self, *args, **kwargs):
        super(EntradaMantenimiento, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             persona=self.persona,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
                    
    def __str__(self):
        return "Mantenimiento"
    
    class Meta:
        verbose_name = "Entrada de Mantenimiento"
        verbose_name_plural = "Entradas de Mantenimiento"


class EntradaPiso(Entrada):
    """
    El modelo que recoge la información de una entrada
    de una cuota de piso.
    Las aportadoras de una cuota piso son excluvisvamente
    habitantes.
    """
    habitante_piso = models.ForeignKey(HabitantePiso, on_delete=models.PROTECT, related_name="entradas_piso")

    def validate_unique(self, *args, **kwargs):
        super(EntradaPiso, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             habitante_piso=self.habitante_piso,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Piso"
    
    class Meta:
        verbose_name = "Entrada de Piso"
        verbose_name_plural = "Entradas de Piso"


class EntradaAlquiler(Entrada):
    """
    El modelo que recoge la información de una entrada
    de alquiler, ya sea de una habitación que de un piso.
    Las aportadoras de una cuota de alquiler pueden ser
    invitadas o habitantes.
    """
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT, related_name="entradas_alquiler")

    def validate_unique(self, *args, **kwargs):
        super(EntradaAlquiler, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             persona=self.persona,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Alquiler"

    class Meta:
        verbose_name = "Entrada de Alquiler"
        verbose_name_plural = "Entradas de Alquiler"


class EntradaDonativo(Entrada):
    """
    El modelo que recoge la información de una entrada-donación.
    """
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT, related_name="entradas_donativos")

    def validate_unique(self, *args, **kwargs):
        super(EntradaDonativo, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             persona=self.persona,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Donativo"
    
    class Meta:
        verbose_name = "Entrada de Donativo"
        verbose_name_plural = "Entradas de Donativo"


class EntradaBeneficiosEvento(Entrada):
    """
    El modelo que recoge la información de una entrada de
    beneficios de un evento.
    """

    def validate_unique(self, *args, **kwargs):
        super(EntradaBeneficiosEvento, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Beneficios Evento"

    class Meta:
        verbose_name = "Entrada de Beneficios de un Evento"
        verbose_name_plural = "Entradas de Beneficios de un Evento"


class OtraEntrada(Entrada):
    """
    El modelo que recoge la información de cualquier otro
    tipo de entrada no tipificada por el sistema.
    """

    def validate_unique(self, *args, **kwargs):
        super(OtraEntrada, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             comentarios=self.comentarios,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Otras entradas"
    
    class Meta:
        verbose_name = "Otra Entrada"
        verbose_name_plural = "Otras Entradas"


class EntradaEspacioProyectoProductivo(Entrada):
    """
    El modelo que recoge la información de una entrada de
    alquiler de un espacio de un proyecto productivo.
    """
    proyecto_productivo = models.ForeignKey(ProyectoProductivo, on_delete=models.PROTECT, related_name="entradas_espacio")

    def validate_unique(self, *args, **kwargs):
        super(EntradaEspacioProyectoProductivo, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             proyecto_productivo=self.proyecto_productivo,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Alquiler Espacio Proyecto Productivo"
    
    class Meta:
        verbose_name = "Entrada por el Espacio de un Proyecto Productivo"
        verbose_name_plural = "Entradas por el Espacio de un Proyecto Productivo"


class EntradaBeneficioProyectoProductivo(Entrada):
    """
    El modelo que recoge la información de una entrada del
    beneficio de un proyecto productivo.
    """
    proyecto_productivo = models.ForeignKey(ProyectoProductivo, on_delete=models.PROTECT, related_name="entradas_beneficio")

    def validate_unique(self, *args, **kwargs):
        super(EntradaBeneficioProyectoProductivo, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             proyecto_productivo=self.proyecto_productivo,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Beneficio Proyecto Productivo"
    
    class Meta:
        verbose_name = "Entrada de Beneficio de un Proyecto Productivo"
        verbose_name_plural = "Entradas de Beneficios de un Proyecto Productivo"


class EntradaAlquilerTrastero(Entrada):
    """
    El modelo que recoge la información de una entrada de
    alquiler de un trastero.
    """
    persona_trastero = models.ForeignKey(PersonaTrastero, on_delete=models.PROTECT,related_name="entradas_alquiler")

    def validate_unique(self, *args, **kwargs):
        super(EntradaAlquilerTrastero, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             persona_trastero=self.persona_trastero,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Alquiler Trastero"
    
    class Meta:
        verbose_name = "Entrada de Alquiler de un Trastero"
        verbose_name_plural = "Entradas de Alquiler de un Trastero"


class EntradaTransferencia(Entrada):
    """
    El modelo que recoge la información de una entrada de
    tranferencia desde un banco a otro banco.
    """

    def validate_unique(self, *args, **kwargs):
        super(EntradaTransferencia, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter(importe=self.importe,
                                             fecha=self.fecha,
                                             registro=self.registro).exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            "Alguien se te ha adelantado y ya ha insertado esta entrada",
                            ],
                    }
                )
    
    def __str__(self):
        return "Transferencia entre Cuentas"
    
    class Meta:
        verbose_name = "Entrada de Transferencia"
        verbose_name_plural = "Entradas de Transferencia"


