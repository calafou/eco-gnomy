from datetime import datetime, date
from dateutil.relativedelta import relativedelta

from django.db import models

from .cuotas import CuotaMantenimiento
from druids.calculators import MaintenanceCalculator


class Persona(models.Model):
    """
    Clase genérica para habitante, invitado y huesped.
    """
    nombre = models.CharField(max_length=256)
    cuotas_mantenimiento_adheridas = models.ManyToManyField(
        CuotaMantenimiento, related_name='personas'
    )
    fecha_de_entrada = models.DateField()
    fecha_de_salida = models.DateField(blank=True, null=True)

    @property
    def is_active(self):
        return self.fecha_de_salida is None

    def _get_cuotas_for_operations(self):
        cuotas_base = self.cuotas_mantenimiento_adheridas.exclude(
            fecha_desactivacion__lt=self.fecha_de_entrada,
        )
        cuotas = cuotas_base if self.is_active else cuotas_base.filter(
            fecha_activacion__lt=self.fecha_de_salida
        )
        return cuotas.order_by("fecha_activacion")

    @property
    def cuotas_mantenimiento_pagadas(self):
        cuotas = self._get_cuotas_for_operations()
        return MaintenanceCalculator().payed_maintenance_shares(
            self.entradas_mantenimiento.values("importe", "fecha"),
            cuotas.values(
                "importe", "fecha_activacion", "fecha_desactivacion",
            ),
            self.fecha_de_entrada,
            self.fecha_de_salida,
        )

    @property
    def cuotas_mantenimiento_supuestas(self):
        cuotas = self._get_cuotas_for_operations()
        return MaintenanceCalculator().estimated_maintenance_shares(
            cuotas.values(
                "importe", "fecha_activacion", "fecha_desactivacion",
            ),
            self.fecha_de_entrada,
            self.fecha_de_salida,
        )

    @property
    def deuda_mantenimiento(self):
        cuotas = self._get_cuotas_for_operations()
        return MaintenanceCalculator().maintenance_debt(
            self.entradas_mantenimiento.values("importe", "fecha"),
            cuotas.values(
                "importe", "fecha_activacion", "fecha_desactivacion",
            ),
            self.fecha_de_entrada,
            self.fecha_de_salida,
        )

    @property
    def tiene_pacto_trastero(self):
        return self.personatrastero_set.count() != 0

    @property
    def estado_pactos_trastero(self):
        estado_pactos = []
        for pacto in self.personatrastero_set.all():
            estado_pactos += [
                {
                    'nombre': pacto.trastero.nombre,
                    'usando': pacto.is_active,
                    'fecha_entrada': pacto.fecha_entrada,
                    'fecha_salida': pacto.fecha_salida,
                    'cuotas_pagadas': pacto.cuotas_pagadas,
                    'deuda': pacto.deuda,
                    'comentarios': pacto.comentarios,
                    'ultimos_movimientos': pacto.ultimos_movimientos,
                }
            ]
        return estado_pactos

    @property
    def ultimos_movimientos_mantenimiento(self):
        return self.entradas_mantenimiento.all().order_by("-fecha")[:20]

    def __str__(self):
        return self.nombre


class Habitante(Persona):
    """
    El modelo que recoge la información sobre una habitante.
    Una habitante es aquella persona que reside
    permanentemente en el territorio.
    Ya sea en viviendas, casa roja o el camping.
    Tendrá una cuota de mantenimiento de 10€ al mes,
    con posibilidad de modificarla.
    """
    @property
    def tiene_pacto_de_piso(self):
        return self.habitantepiso_set.count() != 0

    @property
    def tiene_movimientos_alquiler(self):
        return self.entradas_alquiler.all().exists()

    @property
    def ultimos_movimientos_alquiler(self):
        return self.entradas_alquiler.all().order_by("-fecha")[:20]

    @property
    def estado_pactos_de_piso(self):
        estado_pactos = []
        for pacto in self.habitantepiso_set.all():
            estado_pactos += [
                {
                    'nombre': pacto.piso.nombre,
                    'viviendo': pacto.is_active,
                    'fecha_entrada': pacto.fecha_entrada,
                    'fecha_salida': pacto.fecha_salida,
                    'cuotas_pagadas': pacto.cuotas_pagadas,
                    'deuda': pacto.deuda,
                    'porcentaje_pagado': pacto.porcentaje_pagado,
                    'total_acumulado': pacto.total_acumulado,
                    'cuotas_supuestas': pacto.cuotas_supuestas,
                    'entrada_pagada': pacto.entrada_pagada,
                    'onduline_pagado': pacto.onduline_pagado,
                    'comentarios': pacto.comentarios,
                    'ultimos_movimientos': pacto.ultimos_movimientos,
                }
            ]
        return estado_pactos


class Invitada(Persona):
    """
    El modelo que recoge la información sobre una invitada.
    Una invitada es aquella persona que reside temporalmente
    en el territorio.
    Aunque su estancia es mínimo de un mes.
    Tendrá una cuota de mantenimiento de 15€ al mes,
    con posiblidad de modificarla.
    Una invitada puede convertirse en habitante.
    Para esto se cierra la estancia como invitada
    y se crea una habitante nueva.
    """
    pass


class Huesped(Persona):
    """
    El modelo que recoge la información sobre una huesped.
    Una huesped es aquella persona que reside temporalmente
    en el territorio.
    Aunque su estancia es de duración incierta o inferior al mes.
    Tendrá una cuota de mantenimiento de 30€ al mes,
    con posibilidad de modificarla.
    Una huesped puede convertirse en invitada si hace un pacto
    claro con la asamblea.
    Para esto se cierra la estancia como huesped y se crea una
    invitada nueva.
    """
    class Meta:
        verbose_name_plural = "Huespedes"

