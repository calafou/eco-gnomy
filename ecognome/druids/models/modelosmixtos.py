from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal as D

from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce

from .espacios import Trastero, Piso
from .personas import Persona, Habitante
from druids.calculators import HousingCalculator
from druids.calculators import StorageCalculator


class PersonaTrastero(models.Model):
    """
    El modelo que recoge la información preciosa de la
    relación entre un Trastero y una Persona.
    """
    trastero = models.ForeignKey(Trastero, on_delete=models.PROTECT)
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    fecha_entrada = models.DateField(verbose_name="Fecha de entrada")
    fecha_salida = models.DateField(blank=True, null=True, verbose_name="Fecha de salida")
    comentarios = models.TextField(blank=True, max_length=2048, verbose_name="Comentarios de AG Economia")

    @property
    def is_active(self):
        return self.fecha_salida is None

    @property
    def cuotas_pagadas(self):
        return StorageCalculator().payed_storage_shares(
            self.entradas_alquiler.values("importe"),
            self.trastero.cuota_trastero,
        )

    @property
    def deuda(self):
        return StorageCalculator().storage_debt(
            self.entradas_alquiler.values("importe"),
            self.trastero.cuota_trastero,
            self.fecha_entrada,
            self.fecha_salida,
        )

    @property
    def ultimos_movimientos(self):
        return self.entradas_alquiler.all().order_by("-fecha")[:20]
    
    def __str__(self):
        return ("%s - %s" % (self.trastero.nombre, self.persona.nombre))


class HabitantePiso(models.Model):
    """
    El modelo que recoge la información preciosa de la
    relación entre un Piso y una Habitante.
    """
    habitante = models.ForeignKey(Habitante, on_delete=models.PROTECT)
    piso = models.ForeignKey(Piso, on_delete=models.PROTECT)
    fecha_entrada = models.DateField(verbose_name="Fecha de entrada")
    fecha_salida = models.DateField(blank=True, null=True, verbose_name="Fecha de salida")
    comentarios = models.TextField(blank=True, max_length=2048, verbose_name="Comentarios de AG Economia")

    @property
    def is_active(self):
        return self.fecha_salida is None

    @property
    def entrada_pagada(self):
        return HousingCalculator().has_entrance_share(
            self.entradas_piso.values("importe"),
            dict(
                importe=self.piso.cuota_piso.importe,
                cuota_entrada=self.piso.cuota_piso.cuota_entrada,
                cuota_onduline=self.piso.cuota_piso.cuota_onduline,
            ),
        )

    @property
    def onduline_pagado(self):
        return HousingCalculator().has_onduline_share(
            self.entradas_piso.values("importe"),
            dict(
                importe=self.piso.cuota_piso.importe,
                cuota_entrada=self.piso.cuota_piso.cuota_entrada,
                cuota_onduline=self.piso.cuota_piso.cuota_onduline,
            ),
        )

    @property
    def cuotas_pagadas(self):
        entradas = self.entradas_piso.values("importe")
        cuota = dict(
            importe=self.piso.cuota_piso.importe,
            cuota_entrada=self.piso.cuota_piso.cuota_entrada,
            cuota_onduline=self.piso.cuota_piso.cuota_onduline,
        )

        return HousingCalculator().payed_housing_shares(entradas, cuota)

    @property
    def cuotas_supuestas(self):
        return HousingCalculator().estimated_housing_shares(
            self.fecha_entrada, self.fecha_salida
        )

    @property
    def deuda(self):
        return HousingCalculator().housing_debt(
            self.entradas_piso.values("importe"),
            dict(
                importe=self.piso.cuota_piso.importe,
                cuota_entrada=self.piso.cuota_piso.cuota_entrada,
                cuota_onduline=self.piso.cuota_piso.cuota_onduline,
            ),
            self.fecha_entrada,
            self.fecha_salida,
        )

    @property
    def porcentaje_pagado(self):
        return HousingCalculator().percentage_paid(
            self.entradas_piso.values("importe"),
            self.piso.valor_total,
        )

    @property
    def total_acumulado(self):
        return self.entradas_piso.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]

    @property
    def ultimos_movimientos(self):
        return self.entradas_piso.all().order_by("-fecha")[:20]
    
    def __str__(self):
        return ("%s - %s" % (self.piso.nombre, self.habitante.nombre))
