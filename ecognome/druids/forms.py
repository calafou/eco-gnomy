from django import forms
from druids.models import Habitante, ProyectoProductivo, Entrada, Salida


class SelectHabitanteForm(forms.Form):
    """
    Just a single select field with all the habitants.
    """
    habitante = forms.ModelChoiceField(required=True, label="Escoge un habitante",
                                       queryset=Habitante.objects.all().order_by("nombre"))


class SelectProyectoProductivoForm(forms.Form):
    """
    Just a single select field with all projects.
    """
    proyecto = forms.ModelChoiceField(required=True, label="Escoge un proyecto",
                                      queryset=ProyectoProductivo.objects.all().order_by("nombre"))


class SelectEntradaTipoForm(forms.Form):
    """
    Just a single select field with all kind of incomes
    """
    tipo_entrada = forms.ChoiceField(required=True, label="Escoge un tipo de entrada",
                                     choices=Entrada.TIPOS_ENTRADA)


class SelectSalidaTipoForm(forms.Form):
    """
    Just a single select field with all kind of outcomes
    """
    tipo_salida = forms.ChoiceField(required=True, label="Escoge un tipo de salida",
                                    choices=Salida.TIPOS_SALIDA)



