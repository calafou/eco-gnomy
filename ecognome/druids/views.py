from datetime import datetime, timedelta, date
from decimal import Decimal as D
from itertools import chain
from operator import attrgetter

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.forms.models import modelform_factory, modelformset_factory
from django.forms.widgets import Select
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader, Context
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.views.generic.list import ListView

from . import forms
from . import mixins
from . import models

from .models import CuotaMantenimiento, CuotaPiso, \
    Piso, Trastero, \
    Metalico, Banco, PayPal, Registro, \
    ProyectoProductivo, \
    Habitante, Invitada, Huesped, Persona, \
    HabitantePiso, PersonaTrastero, \
    Salida, \
    EntradaMantenimiento, EntradaPiso, EntradaAlquiler, \
    EntradaDonativo, EntradaBeneficiosEvento, OtraEntrada, \
    EntradaEspacioProyectoProductivo, EntradaBeneficioProyectoProductivo, \
    EntradaAlquilerTrastero, EntradaTransferencia, Entrada, \
    Balance, ReporteMensualPorTipo


class HomeView(mixins.TitleMixin, TemplateView):
    """
    Public home view.
    """

    template_name = "druids/index.html"
    title = "Vista Aerea"

    def _get_internal_debt(self):
        housing_debt = sum(
            map(
                lambda x: x.deuda,
                models.HabitantePiso.objects.all(),
            )
        )
        maintenance_debt = sum(
            map(
                lambda x: x.deuda_mantenimiento,
                models.Habitante.objects.all(),
            )
        )
        storage_debt = sum(
            map(
                lambda x: x.deuda,
                models.PersonaTrastero.objects.all(),
            )
        )
        projects_debt = sum(
            map(
                lambda x: x.deuda_espacio,
                models.ProyectoProductivo.objects.all(),
            )
        )
        return housing_debt + maintenance_debt + storage_debt + projects_debt

    def _get_contributing_people(self):
        return models.Habitante.objects.filter(fecha_de_salida__isnull=True).count()

    def _get_active_projects(self):
        return models.ProyectoProductivo.objects.filter(
            fecha_disolucion__isnull=True
        ).count()

    def _get_total_balance(self):
        incomes_subtotal = models.Entrada.objects.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]
        outcomes_subtotal = models.Salida.objects.aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]
        return incomes_subtotal - outcomes_subtotal

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        data["internal_debt"] = self._get_internal_debt()
        data["active_contributors"] = self._get_contributing_people()
        data["active_projects"] = self._get_active_projects()
        data["total_balance"] = self._get_total_balance()
        return data


class SituacionesPersonalesView(
        mixins.BreadCrumbsMixin, mixins.TitleMixin, FormView
):
    """
    This view renders a form that is used to update the content
    of the view. If `habitante_id` is None only the form will be rendered.
    Else, all the relevant information about specific `Habitante` will be rendered.
    """
    form_class = forms.SelectHabitanteForm
    template_name = "druids/situaciones_personales.html"
    title = "Situaciones Personales"

    raw_breadcrumbs = [
        {
            "title": "Vista Aérea",
            "is_active": False,
            "url": reverse_lazy("druids:index"),
        },
        {
            "title": "Situaciones Personales",
            "is_active": True,
            "url": None,
        }
    ]

    def form_valid(self, form):
        selected = form.cleaned_data['habitante']
        habitante_id = selected.id
        return redirect(
            'druids:situaciones-personales',
            habitante_id=habitante_id
        )

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        if "habitante_id" not in self.kwargs:
            return data
        habitante = get_object_or_404(
            models.Habitante, id=self.kwargs["habitante_id"],
        )
        data["habitante"] = habitante
        return data


class ProyectosProductivosView(
        mixins.BreadCrumbsMixin, mixins.TitleMixin, FormView,
):
    form_class = forms.SelectProyectoProductivoForm
    template_name = "druids/proyectos_productivos.html"
    title = "Proyectos Productivos"

    raw_breadcrumbs = [
        {
            "title": "Vista Aérea",
            "is_active": False,
            "url": reverse_lazy("druids:index"),
        },
        {
            "title": "Proyectos Productivos",
            "is_active": True,
            "url": None,
        }
    ]

    def form_valid(self, form):
        selected = form.cleaned_data["proyecto"]
        proyecto_id = selected.id
        return redirect(
            "druids:proyectos-productivos",
            proyecto_id=proyecto_id,
        )

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        if "proyecto_id" not in self.kwargs:
            return data
        proyecto = get_object_or_404(
            models.ProyectoProductivo, id=self.kwargs["proyecto_id"],
        )
        data["proyecto"] = proyecto
        return data


def balance_general(request):
    """
    Main balance in accounting view.
    """
    title = "Balance general"

    total = str(sum([entrada.importe for entrada in Entrada.objects.all()]) - sum([salida.importe for salida in Salida.objects.all()]))

    return render(request,
                  'druids/balance_general.html',
                  {
                      'title': title,
                      'total': total,
                  }
              )

             
@login_required
def gt_dashboard(request):
    """
    Dashboard for GT Economía
    """
    return render(
        request,
        'druids/gt-dashboard.html'
        )


@login_required
def rectificar_salida(request, salida_id):
    """
    Custom view that corrects a bad outgoing
    entry.
    """

    salida = get_object_or_404(Salida, id=salida_id)

    try:
        with transaction.atomic():
            salida.comentarios = salida.comentarios + " (rectificada)" if salida.comentarios else "(rectificada)"
            salida.save()
            Salida.objects.create(
                fecha=datetime.now().date(),
                importe=salida.importe * D("-1"),
                tipo_salida=salida.tipo_salida,
                comentarios="Rectificando salida del %s con importe %s" % (str(salida.fecha), str(salida.importe)),
                usuaria_economia=request.user,
                registro=salida.registro,
            )
            messages.success(request, "Se ha rectificado correctamente la salida")
    except Exception as e:
        messages.error(request, "Hemos encontrado el siguiente error: %s" % e)

    return redirect(
        "druids:extracto-registro", registro_id=salida.registro.id
    )


@login_required
def rectificar_entrada(request, entrada_id):
    """
    Custom view that corrects a bad incoming
    entry.
    """

    entrada = get_object_or_404(Entrada, id=entrada_id)

    try:
        with transaction.atomic():
            entrada.comentarios = entrada.comentarios + " (rectificada)" if entrada.comentarios else "(rectificada)"
            entrada.save()
            if entrada.tipo == "mantenimiento":
                EntradaMantenimiento.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    persona=entrada.entradamantenimiento.persona,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "piso":
                EntradaPiso.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    habitante_piso=entrada.entradapiso.habitante_piso,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "alquiler":
                EntradaAlquiler.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    persona=entrada.entradaalquiler.persona,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "donativo":
                EntradaDonativo.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    persona=entrada.entradadonativo.persona,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "evento":
                EntradaBeneficiosEvento.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "espacio_pp":
                EntradaEspacioProyectoProductivo.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    proyecto_productivo=entrada.entradaespacioproyectoproductivo.proyecto_productivo,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "beneficio_pp":
                EntradaBeneficioProyectoProductivo.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    proyecto_productivo=entrada.entradabeneficioproyectoproductivo.proyecto_productivo,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "trastero":
                EntradaAlquilerTrastero.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    persona_trastero=entrada.entradaalquilertrastero.persona_trastero,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "otra":
                OtraEntrada.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            elif entrada.tipo == "trasnferencia":
                EntradaTransferencia.objects.create(
                    fecha=datetime.now().date(),
                    importe=entrada.importe * D("-1"),
                    tipo=entrada.tipo,
                    registro=entrada.registro,
                    usuaria_economia=request.user,
                    comentarios="Rectificando entrada del %s con importe %s" % (str(entrada.fecha), str(entrada.importe)),
                )
            else:
                raise
            messages.success(request, "Se ha rectificado correctamente la entrada")

    except Exception as e:
        messages.error(request, "Hemos encontrado el siguiente error: %s" % e)

    return redirect(
        "druids:extracto-registro", registro_id=entrada.registro.id
    )


@login_required
def reportes_mensuales_por_tipo(request):
    """
    Custom view that renders all of the monthly reports instances
    as a summary. It also takes care of creating necessary instances
    if not created
    """
    title = "Resumen reportes mensuales"

    now = datetime.now()
    initial_date = date(now.year, now.month, 1)
    end_date = date(initial_date.year, initial_date.month+1, 1) - timedelta(days=1)
    if not ReporteMensualPorTipo.objects.filter(fecha_de_inicio=initial_date).exists():
        ReporteMensualPorTipo.objects.create(
            fecha_de_inicio=initial_date,
            fecha_de_final=end_date,
            )
    reports = ReporteMensualPorTipo.objects.all().order_by('-fecha_de_final')

    return render(
        request,
        'druids/estadisticas/reportes_mensuales_por_tipo.html',
        {
            'title': title,
            'reports': reports,
        }
    )


class ReporteMensualPorTipoDetailView(DetailView):
    """
    Detail View for ReporteMensualPorTipo model.
    """
    model = ReporteMensualPorTipo
    template_name = 'druids/estadisticas/reporte_mensual_por_tipo.html'
    context_object_name = 'report'
    
    title = 'Reporte mensual'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ReporteMensualPorTipoDetailView, self).dispatch(*args, **kwargs)


class CuotaMantenimientoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for CuotaMantenimiento
    model.
    """
    model = CuotaMantenimiento
    template_name = 'druids/cuotas/crear_mantenimiento.html'
    form_class = modelform_factory(
        CuotaMantenimiento,
        fields=['descripcion', 'tipo', 'importe', 'fecha_activacion']
                               )
    success_url = '/gt-economia/'
    success_message = 'La cuota de mantenimiento se ha creado correctamente.'

    title = 'Crear cuota de mantenimiento'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaMantenimientoCreateView, self).dispatch(*args, **kwargs)


class CuotaMantenimientoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for CuotaMantenimiento model.
    """
    model = CuotaMantenimiento
    template_name = "druids/cuotas/editar-cuota-mantenimiento.html"
    fields = ['fecha_desactivacion']

    title = "Desactivar cuota mantenimiento"

    success_url = "/gt-economia/"
    success_message = "La cuota de mantenimiento se ha desactivado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaMantenimientoUpdateView, self).dispatch(*args, **kwargs)


class CuotaMantenimientoListView(ListView):
    """
    List View for CuotaMantenimiento model.
    """
    context_object_name = 'cuotas'
    queryset = CuotaMantenimiento.objects.all().order_by('tipo')
    template_name = "druids/cuotas/lista_mantenimiento.html"
    title = 'Lista de cuotas de mantenimiento'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaMantenimientoListView, self).dispatch(*args, **kwargs)


class CuotaPisoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for CuotaPiso
    model.
    """
    model = CuotaPiso
    template_name = 'druids/cuotas/crear_cuota_piso.html'
    form_class = modelform_factory(
        CuotaPiso,
        fields=['importe', 'descripcion', 'cuota_entrada', 'cuota_onduline']
    )
    success_url = '/gt-economia/'
    success_message = 'La cuota de piso se ha creado correctamente.'

    title = 'Crear cuota de piso'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaPisoCreateView, self).dispatch(*args, **kwargs)


class CuotaPisoListView(ListView):
    """
    List View for CuotaMantenimiento model.
    """
    context_object_name = 'cuotas'
    queryset = CuotaPiso.objects.all().order_by('importe')
    template_name = "druids/cuotas/lista_cuotas_piso.html"
    title = 'Lista de cuotas de piso'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaPisoListView, self).dispatch(*args, **kwargs)


class PisoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Piso model.
    """
    model = Piso
    template_name = "druids/espacios/crear-piso.html"
    form_class = modelform_factory(Piso,
                                   fields=['nombre', 'valor_total', 'cuota_piso']
                               )
    success_url = '/gt-economia/'
    success_message = 'El piso se ha creado correctamente.'

    title = 'Crear piso'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PisoCreateView, self).dispatch(*args, **kwargs)


class PisoListView(ListView):
    """
    List View for Piso model.
    """
    context_object_name = 'pisos'
    queryset = Piso.objects.all().order_by('nombre')
    template_name = "druids/espacios/lista-pisos.html"
    title = "Lista de pisos"

    edit_url = "druids:editar-piso"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PisoListView, self).dispatch(*args, **kwargs)


class PisoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Piso model.
    """
    model = Piso
    template_name = "druids/espacios/crear-piso.html"
    fields = ['nombre', 'valor_total', 'cuota_piso']
    
    title = "Modificar piso"

    success_url = "/gt-economia/"
    success_message = "El piso se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PisoUpdateView, self).dispatch(*args, **kwargs)


class TrasteroCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Trastero model.
    """
    model = Trastero
    template_name = "druids/espacios/crear-trastero.html"
    form_class = modelform_factory(Trastero,
                                   fields=['nombre', 'cuota_trastero']
                               )
    success_url = '/gt-economia/'
    success_message = 'El trastero se ha creado correctamente.'

    title = 'Crear trastero'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TrasteroCreateView, self).dispatch(*args, **kwargs)


class TrasteroListView(ListView):
    """
    List View for Trastero model.
    """
    context_object_name = 'trasteros'
    queryset = Trastero.objects.all().order_by('nombre')
    template_name = "druids/espacios/lista-trasteros.html"
    title = "Lista de trasteros"

    edit_url = "druids:editar-trastero"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TrasteroListView, self).dispatch(*args, **kwargs)


class TrasteroUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Trastero model.
    """
    model = Trastero
    template_name = "druids/espacios/crear-trastero.html"
    fields = ['nombre', 'cuota_trastero']
    
    title = "Modificar trastero"

    success_url = "/gt-economia/"
    success_message = "El trastero se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TrasteroUpdateView, self).dispatch(*args, **kwargs)


class MetalicoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Metalico model.
    """
    model = Metalico
    template_name = "druids/registros/crear_registro.html"
    form_class = modelform_factory(Metalico,
                                   fields=["nombre"]
                               )

    title = "Crear registro de metálico"
    
    success_url = "/gt-economia/"
    success_message = "El registro de metálico se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MetalicoCreateView, self).dispatch(*args, **kwargs)
    

class MetalicoListView(ListView):
    """
    List View for Metalico model.
    """
    context_object_name = "registros"
    queryset = Metalico.objects.all().order_by('nombre')
    template_name = "druids/registros/lista-registros.html"
    title = "Lista de registros de metálico"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MetalicoListView, self).dispatch(*args, **kwargs)


class BancoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Banco model.
    """
    model = Banco
    template_name = "druids/registros/crear_registro.html"
    form_class = modelform_factory(Banco,
                                   fields=["nombre"]
                               )

    title = "Crear registro bancario"
    
    success_url = "/gt-economia/"
    success_message = "El registro bancario se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BancoCreateView, self).dispatch(*args, **kwargs)
    

class BancoListView(ListView):
    """
    List View for Banco model.
    """
    context_object_name = "registros"
    queryset = Banco.objects.all().order_by('nombre')
    template_name = "druids/registros/lista-registros.html"
    title = "Lista de registros bancarios"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BancoListView, self).dispatch(*args, **kwargs)


class PaypalCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for PayPal model.
    """
    model = PayPal
    template_name = "druids/registros/crear_registro.html"
    form_class = modelform_factory(PayPal,
                                   fields=["nombre"]
                               )

    title = "Crear registro electrónico"
    
    success_url = "/gt-economia/"
    success_message = "El registro electrónico se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PaypalCreateView, self).dispatch(*args, **kwargs)
    

class PaypalListView(ListView):
    """
    List View for PayPal model.
    """
    context_object_name = "registros"
    queryset = PayPal.objects.all().order_by('nombre')
    template_name = "druids/registros/lista-registros.html"
    title = "Lista de registros electrónicos"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PaypalListView, self).dispatch(*args, **kwargs)


@login_required
def cuaderno_contable_entradas(request, tipo_entrada=None):
    """
    List view for incomes as a journal
    """

    title = "Cuadernos contables de entradas"

    entradas = []

    if request.method == 'POST':
        form = forms.SelectEntradaTipoForm(request.POST)
        if form.is_valid():
            selected = form.cleaned_data['tipo_entrada']
            return redirect('druids:cuaderno_contable_entradas', tipo_entrada=selected)

    else:
        form = forms.SelectEntradaTipoForm(initial={"tipo_entrada": tipo_entrada})
        if tipo_entrada:
            entradas = Entrada.objects.filter(tipo=tipo_entrada).order_by("-fecha")

    return render(request,
                  'druids/cuaderno_contable_entradas.html',
                  {
                      'title': title,
                      'entradas': entradas,
                      'tipo_entrada': tipo_entrada,
                      'form': form,
                  })


@login_required
def cuaderno_contable_salidas(request, tipo_salida=None):
    """
    List view for outcomes as a journal
    """

    title = "Cuadernos contables de salidas"

    salidas = []

    if request.method == 'POST':
        form = forms.SelectSalidaTipoForm(request.POST)
        if form.is_valid():
            selected = form.cleaned_data['tipo_salida']
            return redirect('druids:cuaderno_contable_salidas', tipo_salida=selected)

    else:
        form = forms.SelectSalidaTipoForm(initial={"tipo_salida": tipo_salida})
        if tipo_salida:
            salidas = Salida.objects.filter(tipo_salida=tipo_salida).order_by("-fecha")

    return render(request,
                  'druids/cuaderno_contable_salidas.html',
                  {
                      'title': title,
                      'salidas': salidas,
                      'tipo_salida': tipo_salida,
                      'form': form,
                  })


@login_required
def extracto_registro(request, registro_id=None):
    """
    List view for account statements.
    Takes in consideration just current year.
    """

    title = "Extracto registro"
    
    registro = get_object_or_404(Registro, id=registro_id)
    
    entradas = registro.entrada_set.all()

    salidas = registro.salida_set.all()

    movimientos = sorted(chain(entradas, salidas), key=attrgetter('fecha'))

    lineas = list(map(lambda key, value: {'numero': key, 'movimiento': value}, range(len(movimientos)), movimientos))

    return render(request,
                  'druids/registros/extracto-registro.html',
                  { 'title': title,
                    'lineas': lineas,
                  })

    
class ProyectoProductivoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for ProyectoProductivo model.
    """
    model = ProyectoProductivo
    template_name = "druids/proyectos/crear-proyecto.html"
    form_class = modelform_factory(ProyectoProductivo,
                                   fields=["nombre", "cuota_espacio", "fecha_creacion"]
                               )

    title = "Crear proyecto productivo"

    success_url = "/gt-economia/"
    success_message = "El proyecto productivo se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProyectoProductivoCreateView, self).dispatch(*args, **kwargs)


class ProyectoProductivoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for ProyectoProductivo model.
    """
    model = ProyectoProductivo
    template_name = "druids/proyectos/editar-proyecto.html"
    fields = ['cuota_espacio', 'fecha_disolucion']
    
    title = "Modificar proyecto"

    success_url = "/gt-economia/"
    success_message = "El proyecto se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProyectoProductivoUpdateView, self).dispatch(*args, **kwargs)


class ProyectoProductivoListView(ListView):
    """
    List View for ProyectoProductivo model.
    """
    context_object_name = "proyectos"
    queryset = ProyectoProductivo.objects.all().order_by('nombre')
    template_name = "druids/proyectos/lista-proyectos.html"
    title = "Lista de proyectos productivos"

    edit_url = "druids:editar-proyecto"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProyectoProductivoListView, self).dispatch(*args, **kwargs)
    

class HabitanteCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Habitante model.
    """
    model = models.Habitante
    template_name = "druids/personas/crear-persona.html"
    form_class = modelform_factory(
        models.Habitante,
        fields=[
            "nombre",
            "cuotas_mantenimiento_adheridas",
            "fecha_de_entrada",
        ]
    )

    title = "Crear habitante"

    success_url = "/gt-economia/"
    success_message = "La habitante se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitanteCreateView, self).dispatch(*args, **kwargs)


class HabitanteUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Habitante model.
    """
    model = models.Habitante
    template_name = "druids/personas/editar-persona.html"
    fields = ['cuotas_mantenimiento_adheridas', 'fecha_de_salida']
    
    title = "Modificar habitante"

    success_url = "/gt-economia/"
    success_message = "La habitante se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitanteUpdateView, self).dispatch(*args, **kwargs)


class HabitanteListView(ListView):
    """
    List View for Habitante model.
    """
    context_object_name = "personas"
    queryset = models.Habitante.objects.all().order_by('nombre')
    template_name = "druids/personas/lista-personas.html"
    title = "Lista de habitantes"

    edit_url = "druids:editar-habitante"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitanteListView, self).dispatch(*args, **kwargs)
    

class InvitadaCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Invitada model.
    """
    model = Invitada
    template_name = "druids/personas/crear-persona.html"
    form_class = modelform_factory(
        Invitada,
        fields=[
            "nombre",
            "cuotas_mantenimiento_adheridas",
            "fecha_de_entrada",
        ]
    )

    title = "Crear invitada"

    success_url = "/gt-economia/"
    success_message = "La invitada se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(InvitadaCreateView, self).dispatch(*args, **kwargs)


class InvitadaUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Invitada model.
    """
    model = Invitada
    template_name = "druids/personas/editar-persona.html"
    fields = ['cuota_mantenimiento', 'fecha_de_salida']
    
    title = "Modificar invitada"

    success_url = "/gt-economia/"
    success_message = "La invitada se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(InvitadaUpdateView, self).dispatch(*args, **kwargs)


class InvitadaListView(ListView):
    """
    List View for Invitada model.
    """
    context_object_name = "personas"
    queryset = Invitada.objects.all().order_by('nombre')
    template_name = "druids/personas/lista-personas.html"
    title = "Lista de invitadas"

    edit_url = "druids:editar-invitada"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(InvitadaListView, self).dispatch(*args, **kwargs)


class HuespedCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Huesped model.
    """
    model = Huesped
    template_name = "druids/personas/crear-persona.html"
    form_class = modelform_factory(
        Huesped,
        fields=[
            "nombre",
            "cuotas_mantenimiento_adheridas",
            "fecha_de_entrada",
        ]
    )

    title = "Crear huesped"

    success_url = "/gt-economia/"
    success_message = "La huesped se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HuespedCreateView, self).dispatch(*args, **kwargs)


class HuespedUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Huesped model.
    """
    model = Huesped
    template_name = "druids/personas/editar-persona.html"
    fields = ['cuota_mantenimiento', 'fecha_de_salida']
    
    title = "Modificar huesped"

    success_url = "/gt-economia/"
    success_message = "La huesped se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HuespedUpdateView, self).dispatch(*args, **kwargs)


class HuespedListView(ListView):
    """
    List View for Huesped model.
    """
    context_object_name = "personas"
    queryset = Huesped.objects.all().order_by('nombre')
    template_name = "druids/personas/lista-personas.html"
    title = "Lista de huéspedes"

    edit_url = "druids:editar-huesped"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HuespedListView, self).dispatch(*args, **kwargs)


class HabitantePisoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for HabitantePiso model.
    """
    model = HabitantePiso
    template_name = "druids/pactos/crear-habitantepiso.html"
    form_class = modelform_factory(
        HabitantePiso,
        fields=["habitante", "piso", "fecha_entrada"]
    )

    title = "Crear pacto de reciprocidad"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitantePisoCreateView, self).dispatch(*args, **kwargs)


class HabitantePisoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for HabitantePiso model.
    """
    model = HabitantePiso
    template_name = "druids/pactos/editar-pacto.html"
    fields = ['fecha_salida', "comentarios"]

    kind_of = "piso"
    
    title = "Editar pacto"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha editado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitantePisoUpdateView, self).dispatch(*args, **kwargs)


class HabitantePisoListView(ListView):
    """
    List View for HabitantePiso model.
    """
    context_object_name = "pactos"
    queryset = HabitantePiso.objects.all().order_by('fecha_entrada')
    template_name = "druids/pactos/lista-habitantepiso.html"
    title = "Lista de pactos"

    edit_url = "druids:editar-habitantepiso"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitantePisoListView, self).dispatch(*args, **kwargs)


class PersonaTrasteroCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for PersonaTrastero model.
    """
    model = PersonaTrastero
    template_name = "druids/pactos/crear-personatrastero.html"
    form_class = modelform_factory(PersonaTrastero,
                                   fields=["persona", "trastero",
                                           "fecha_entrada"]
                               )

    title = "Crear pacto de reciprocidad"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonaTrasteroCreateView, self).dispatch(*args, **kwargs)


class PersonaTrasteroUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for PersonaTrastero model.
    """
    model = PersonaTrastero
    template_name = "druids/pactos/editar-pacto.html"
    fields = ['fecha_salida', "comentarios"]

    kind_of = "trastero"
    
    title = "Concluir pacto"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha cerrado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonaTrasteroUpdateView, self).dispatch(*args, **kwargs)


class PersonaTrasteroListView(ListView):
    """
    List View for PersonaTrastero model.
    """
    context_object_name = "pactos"
    queryset = PersonaTrastero.objects.all().order_by('fecha_entrada')
    template_name = "druids/pactos/lista-personatrastero.html"
    title = "Lista de pactos"

    edit_url = "druids:editar-personatrastero"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonaTrasteroListView, self).dispatch(*args, **kwargs)


@login_required
def insertar_salidas(request):
    """
    Custom view to insert 'Salidas'
    """

    title = "Insertar salidas"
    
    SalidasModelFormSet = modelformset_factory(Salida, extra=9, min_num=1, validate_min=True,
                                               fields=["fecha", "tipo_salida",
                                           "importe", "comentarios",
                                           "registro"],
                                           )
    if request.method == 'POST':
        formset = SalidasModelFormSet(data=request.POST)
        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.usuaria_economia = request.user
                instance.save()
            messages.success(request, "Las salidas se han insertado correctamente.")
            return redirect("druids:gt-dashboard")

    else:
        formset = SalidasModelFormSet(queryset=Salida.objects.none())

    return render(
        request,
        "druids/movimientos/crear-salidas.html",
        {'formset': formset,
         'title': title}
        )


@login_required
def insertar_entradas_otras(request):
    """
    Custom view to insert:
    * EntradaMantenimiento
    * EntradaDonativo
    * OtraEntrada
    * Transferencia
    """

    title = "Insertar otras entradas"

    MantenimientoModelFormSet = modelformset_factory(EntradaMantenimiento,
                                                     exclude=['tipo',
                                                              'usuaria_economia'])

    DonativoModelFormSet = modelformset_factory(EntradaDonativo,
                                                exclude=['tipo',
                                                         'usuaria_economia'])

    OtrasModelFormSet = modelformset_factory(OtraEntrada,
                                             exclude=['tipo',
                                                      'usuaria_economia'])

    TransferenciaModelFormSet = modelformset_factory(EntradaTransferencia,
                                                     exclude=['tipo',
                                                              'usuaria_economia'])

    if request.method == 'POST':
        mantenimiento_formset = MantenimientoModelFormSet(request.POST, prefix='mantenimiento')
        donativos_formset = DonativoModelFormSet(request.POST, prefix='donativos')
        otras_formset = OtrasModelFormSet(request.POST, prefix='otras')
        transferencias_formset = TransferenciaModelFormSet(request.POST, prefix='transferencias')

        if mantenimiento_formset.is_valid() and donativos_formset.is_valid() and \
           otras_formset.is_valid() and transferencias_formset.is_valid() :
            cuotas_m = mantenimiento_formset.save(commit=False)
            for cuota in cuotas_m:
                cuota.tipo = 'mantenimiento'
                cuota.usuaria_economia = request.user
                cuota.save()
            donativos = donativos_formset.save(commit=False)
            for donativo in donativos:
                donativo.tipo = 'donativo'
                donativo.usuaria_economia = request.user
                donativo.save()
            otras = otras_formset.save(commit=False)
            for otra in otras:
                otra.tipo = 'otra'
                otra.usuaria_economia = request.user
                otra.save()
            transferencias = transferencias_formset.save(commit=False)
            for transferencia in transferencias:
                transferencia.tipo = 'transferencia'
                transferencia.usuaria_economia = request.user
                transferencia.save()
            messages.success(request, "Las entradas han sido insertadas correctamente")
            return redirect("druids:gt-dashboard")

    else:
        mantenimiento_formset = MantenimientoModelFormSet(queryset=EntradaMantenimiento.objects.none(),
                                                          prefix='mantenimiento')
        donativos_formset = DonativoModelFormSet(queryset=EntradaDonativo.objects.none(),
                                                 prefix='donativos')
        otras_formset = OtrasModelFormSet(queryset=OtraEntrada.objects.none(),
                                          prefix='otras')
        transferencias_formset = TransferenciaModelFormSet(queryset=EntradaTransferencia.objects.none(),
                                                           prefix='transferencias')

    return render(request, "druids/movimientos/crear-otras-entradas.html", {
        'title': title,
        'mantenimiento_formset': mantenimiento_formset,
        'donativos_formset': donativos_formset,
        'otras_formset': otras_formset,
        'transferencias_formset': transferencias_formset
        })


@login_required
def insertar_entradas_alquileres(request):
    """
    Custom view to insert:
    * EntradaPiso
    * EntradaAlquiler
    * EntradaAlquilerTrastero
    """

    title = "Insertar entradas de alquiler"

    PisoModelFormSet = modelformset_factory(EntradaPiso,
                                            exclude=['tipo', 'usuaria_economia'])

    AlquilerModelFormSet = modelformset_factory(EntradaAlquiler,
                                                exclude=['tipo', 'usuaria_economia'])

    AlquilerTrasteroModelFormSet = modelformset_factory(EntradaAlquilerTrastero,
                                                        exclude=['tipo', 'usuaria_economia'])

    if request.method == 'POST':
        pisos_formset = PisoModelFormSet(request.POST, prefix='pisos')
        alquileres_formset = AlquilerModelFormSet(request.POST, prefix='alquileres')
        trasteros_formset = AlquilerTrasteroModelFormSet(request.POST, prefix='trasteros')

        if pisos_formset.is_valid() and alquileres_formset.is_valid() and \
           trasteros_formset.is_valid() :
            pisos = pisos_formset.save(commit=False)
            for piso in pisos:
                piso.tipo = 'piso'
                piso.usuaria_economia = request.user
                piso.save()
            alquileres = alquileres_formset.save(commit=False)
            for alquiler in alquileres:
                alquiler.tipo = 'alquiler'
                alquiler.usuaria_economia = request.user
                alquiler.save()
            trasteros = trasteros_formset.save(commit=False)
            for trastero in trasteros:
                trastero.tipo = 'trastero'
                trastero.usuaria_economia = request.user
                trastero.save()
            messages.success(request, "Las entradas han sido insertadas correctamente")
            return redirect("druids:gt-dashboard")

    else:
        pisos_formset = PisoModelFormSet(queryset=EntradaPiso.objects.none(),
                                         prefix='pisos')
        alquileres_formset = AlquilerModelFormSet(queryset=EntradaAlquiler.objects.none(),
                                                  prefix='alquileres')
        trasteros_formset = AlquilerTrasteroModelFormSet(queryset=EntradaAlquilerTrastero.objects.none(),
                                                         prefix='trasteros')
        
    return render(request, "druids/movimientos/crear-alquileres-entradas.html", {
        'title': title,
        'pisos_formset': pisos_formset,
        'alquileres_formset': alquileres_formset,
        'trasteros_formset': trasteros_formset,
        })


@login_required
def insertar_entradas_proyectos(request):
    """
    Custom view to insert:
    * EntradaEspacioProyectoProductivo
    * EntradaBeneficioProyectoProductivo
    * EntradaBeneficiosEvento
    """

    title = "Insertar entradas de proyectos productivos"

    EspacioProyectoProductivoModelFormSet = modelformset_factory(EntradaEspacioProyectoProductivo,
                                                                 exclude=['tipo', 'usuaria_economia'])
                                                                 

    BeneficioProyectoProductivoModelFormSet = modelformset_factory(EntradaBeneficioProyectoProductivo,
                                                                   exclude=['tipo', 'usuaria_economia'])

    BeneficiosEventoModelFormSet = modelformset_factory(EntradaBeneficiosEvento,
                                                        exclude=['tipo', 'usuaria_economia'])

    if request.method == 'POST':
        espacios_formset = EspacioProyectoProductivoModelFormSet(request.POST, prefix='espacios')
        beneficios_formset = BeneficioProyectoProductivoModelFormSet(request.POST, prefix='beneficios')
        eventos_formset = BeneficiosEventoModelFormSet(request.POST, prefix='eventos')

        if espacios_formset.is_valid() and beneficios_formset.is_valid() and \
           eventos_formset.is_valid() :
            espacios = espacios_formset.save(commit=False)
            for espacio in espacios:
                espacio.tipo = 'espacio_pp'
                espacio.usuaria_economia = request.user
                espacio.save()
            beneficios = beneficios_formset.save(commit=False)
            for beneficio in beneficios:
                beneficio.tipo = 'beneficio_pp'
                beneficio.usuaria_economia = request.user
                beneficio.save()
            eventos = eventos_formset.save(commit=False)
            for evento in eventos:
                evento.tipo = 'evento'
                evento.usuaria_economia = request.user
                evento.save()
            messages.success(request, "Las entradas han sido insertadas correctamente")
            return redirect("druids:gt-dashboard")

    else:
        espacios_formset = EspacioProyectoProductivoModelFormSet(queryset=EntradaEspacioProyectoProductivo.objects.none(),
                                                                 prefix='espacios')
        beneficios_formset = BeneficioProyectoProductivoModelFormSet(queryset=EntradaBeneficioProyectoProductivo.objects.none(),
                                                                     prefix='beneficios')
        eventos_formset = BeneficiosEventoModelFormSet(queryset=EntradaBeneficiosEvento.objects.none(),
                                                       prefix='eventos')
        
    return render(request, "druids/movimientos/crear-proyectos-entradas.html", {
        'title': title,
        'espacios_formset': espacios_formset,
        'beneficios_formset': beneficios_formset,
        'eventos_formset': eventos_formset,
        })


class BalanceCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Balance model.
    """
    model = Balance
    template_name = 'druids/balances/crear-balance.html'
    form_class = modelform_factory(Balance,
                                   fields=['nombre', 'descripcion',
                                           'fecha_de_inicio', 'fecha_de_final']
                               )
    success_url = '/gt-economia/'
    success_message = 'El balance se ha creado correctamente.'

    title = 'Crear balance'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BalanceCreateView, self).dispatch(*args, **kwargs)


class BalanceDetailView(DetailView):
    """
    Detail View for Balance model.
    """
    model = Balance
    template_name = 'druids/balances/ver-balance.html'
    context_object_name = 'balance'
    
    title = 'Ver balance'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BalanceDetailView, self).dispatch(*args, **kwargs)


class BalanceListView(ListView):
    """
    List View for Balance model.
    """
    context_object_name = 'balances'
    queryset = Balance.objects.all().order_by('nombre')
    template_name = "druids/balances/lista-balances.html"
    title = 'Lista de balances'

    edit_url = 'druids:ver-balance'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BalanceListView, self).dispatch(*args, **kwargs)


@login_required
def exportar_entradas_csv(request):
    """
    CSV export feature for Entrada instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=entradas.csv'

    header = [
        "id",
        "fecha",
        "tipo",
        "importe",
        "id_registro",
        "comentarios",
        "id_relacion",
        "nombre_relacion",
        "usuaria_economia",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in Entrada.objects.all().order_by("fecha")]
    
    for entrada in queryset:
        if hasattr(entrada, "entradamantenimiento"):
            nombre_relacion = "con persona"
            id_relacion = entrada.entradamantenimiento.persona.id
        elif hasattr(entrada, "entradapiso"):
            nombre_relacion = "con habitante_piso"
            id_relacion = entrada.entradapiso.habitante_piso.id
        elif hasattr(entrada, "entradaalquiler"):
            nombre_relacion = "con persona"
            id_relacion = entrada.entradaalquiler.persona.id
        elif hasattr(entrada, "entradadonativo"):
            nombre_relacion = "con persona"
            id_relacion = entrada.entradadonativo.persona.id
        elif hasattr(entrada, "entradaespacioproyectoproductivo"):
            nombre_relacion = "con proyecto_productivo"
            id_relacion = entrada.entradaespacioproyectoproductivo.proyecto_productivo.id
        elif hasattr(entrada, "entradabeneficioproyectoproductivo"):
            nombre_relacion = "con proyecto_productivo"
            id_relacion = entrada.entradabeneficioproyectoproductivo.proyecto_productivo.id
        elif hasattr(entrada, "entradaalquilertrastero"):
            nombre_relacion = "con persona_trastero"
            id_relacion = entrada.entradaalquilertrastero.persona_trastero.id
        else:
            nombre_relacion = "---"
            id_relacion = "---"
            
        csv_data += [[
            entrada.id,
            entrada.fecha,
            entrada.tipo,
            entrada.importe,
            entrada.registro.id,
            entrada.comentarios,
            nombre_relacion,
            id_relacion,
            entrada.usuaria_economia,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response
    
    
@login_required
def exportar_salidas_csv(request):
    """
    CSV export feature for Salida instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=salidas.csv'
    
    header = [
        "id",
        "fecha",
        "tipo",
        "importe",
        "id_registro",
        "comentarios",
        "usuaria_economia",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in Salida.objects.all().order_by("fecha")]
    
    for salida in queryset:
            
        csv_data += [[
            salida.id,
            salida.fecha,
            salida.tipo_salida,
            salida.importe,
            salida.registro.id,
            salida.comentarios,
            salida.usuaria_economia,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_pisos_csv(request):
    """
    CSV export feature for Piso instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=pisos.csv'
    
    header = [
        "id",
        "nombre",
        "valor_total",
        "id_cuota_piso",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in Piso.objects.all().order_by("nombre")]
    
    for piso in queryset:
            
        csv_data += [[
            piso.id,
            piso.nombre,
            piso.valor_total,
            piso.cuota_piso.id,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_trasteros_csv(request):
    """
    CSV export feature for Trastero instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=trasteros.csv'
    
    header = [
        "id",
        "nombre",
        "cuota_trastero",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in Trastero.objects.all().order_by("nombre")]
    
    for trastero in queryset:
            
        csv_data += [[
            trastero.id,
            trastero.nombre,
            trastero.cuota_trastero,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_cuotas_piso_csv(request):
    """
    CSV export feature for CuotaPiso instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=cuotas_piso.csv'
    
    header = [
        "id",
        "importe",
        "descripcion",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in CuotaPiso.objects.all().order_by("importe")]
    
    for cuota in queryset:
            
        csv_data += [[
            cuota.id,
            cuota.importe,
            cuota.descripcion,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_cuotas_mantenimiento_csv(request):
    """
    CSV export feature for CuotaMantenimiento instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=cuotas_mantenimiento.csv'
    
    header = [
        "id",
        "importe",
        "descripcion",
        "tipo",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in CuotaMantenimiento.objects.all().order_by("importe")]
    
    for cuota in queryset:
            
        csv_data += [[
            cuota.id,
            cuota.importe,
            cuota.descripcion,
            cuota.tipo,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_habitante_piso_csv(request):
    """
    CSV export feature for HabitantePiso instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=habitante_piso.csv'
    
    header = [
        "id",
        "id_habitante",
        "id_piso",
        "fecha_entrada",
        "fecha_salida",
        "entrada_pagada",
        "onduline_pagado",
        "comentarios",
        "cuotas_pagadas",
        "cuotas_supuestas",
        "deuda",
        "porcentaje_pagado",
        "total_acumulado",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in HabitantePiso.objects.all().order_by("fecha_entrada")]
    
    for pacto in queryset:
            
        csv_data += [[
            pacto.id,
            pacto.habitante.id,
            pacto.piso.id,
            pacto.fecha_entrada,
            pacto.fecha_salida,
            pacto.entrada_pagada,
            pacto.onduline_pagado,
            pacto.comentarios,
            pacto.cuotas_pagadas,
            pacto.cuotas_supuestas,
            pacto.deuda,
            pacto.porcentaje_pagado,
            pacto.total_acumulado,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_persona_trastero_csv(request):
    """
    CSV export feature for PersonaTrastero instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=persona_trastero.csv'
    
    header = [
        "id",
        "id_persona",
        "id_trastero",
        "fecha_entrada",
        "fecha_salida",
        "comentarios",
        "cuotas_pagadas",
        "deuda",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in PersonaTrastero.objects.all().order_by("fecha_entrada")]
    
    for pacto in queryset:
            
        csv_data += [[
            pacto.id,
            pacto.persona.id,
            pacto.trastero.id,
            pacto.fecha_entrada,
            pacto.fecha_salida,
            pacto.comentarios,
            pacto.cuotas_pagadas,
            pacto.deuda,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_personas_csv(request):
    """
    CSV export feature for Persona instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=personas.csv'
    
    header = [
        "id_como_persona",
        "id_como_habitante",
        "nombre",
        "id_cuota_mantenimiento",
        "fecha_de_entrada",
        "fecha_de_salida",
        "cuotas_mantenimiento_pagadas",
        "cuotas_mantenimiento_supuestas",
        "deuda_mantenimiento",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in Persona.objects.all().order_by("nombre")]
    
    for persona in queryset:
        id_como_habitante = "---"
        if hasattr(persona, "habitante"):
            id_como_habitante = persona.habitante.id
        csv_data += [[
            persona.id,
            id_como_habitante,
            persona.nombre,
            persona.cuota_mantenimiento.id,
            persona.fecha_de_entrada,
            persona.fecha_de_salida,
            persona.cuotas_mantenimiento_pagadas,
            persona.cuotas_mantenimiento_supuestas,
            persona.deuda_mantenimiento,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_registros_csv(request):
    """
    CSV export feature for Registro instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=registros.csv'
    
    header = [
        "id",
        "nombre",
        "tipo",
        "saldo",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in Registro.objects.all().order_by("nombre")]
    
    for registro in queryset:
        if hasattr(registro, "metalico"):
            tipo = "metalico"
        elif hasattr(registro, "banco"):
            tipo = "banco"
        elif hasattr(registro, "paypal"):
            tipo = "electronico"
        else:
            tipo = "---"
            
        csv_data += [[
            registro.id,
            registro.nombre,
            tipo,
            registro.saldo,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
def exportar_proyectos_csv(request):
    """
    CSV export feature for ProyectoProductivo instances
    """
        
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=proyectos.csv'
    
    header = [
        "id",
        "nombre",
        "cuota_espacio",
        "fecha_creacion",
        "fecha_disolucion",
        "comentarios",
        "aportacion_alquiler",
        "aportacion_beneficios",
    ]
    
    csv_data = [header]
    
    queryset = [x for x in ProyectoProductivo.objects.all().order_by("nombre")]
    
    for proyecto in queryset:
            
        csv_data += [[
            proyecto.id,
            proyecto.nombre,
            proyecto.cuota_espacio,
            proyecto.fecha_creacion,
            proyecto.fecha_disolucion,
            proyecto.comentarios,
            proyecto.aportacion_alquiler,
            proyecto.aportacion_beneficios,
        ]]
        
    t = loader.get_template("druids/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


### API's views ###

def get_dashboard_data(request):
    """
    It returns a json containing the graph representation
    of the database. It will be used to render
    a circle packing graph.
    """
    data = {
        "name": "Calafou",
        "children": [
            {"name": "Habitantes",
             "children": [{"name": habitante.nombre,
                           "size": 1} for habitante in Habitante.objects.all()]},
            {"name": "Invitadas",
             "children": [{"name": invitada.nombre,
                           "size": 1} for invitada in Invitada.objects.all()]},
            {"name": "Huespedes",
             "children": [{"name": huesped.nombre,
                           "size": 1} for huesped in Huesped.objects.all()]},
            {"name": "Pisos",
             "children": [{"name": piso.nombre,
                           "size": 1} for piso in Piso.objects.all()]},
            {"name": "Trasteros",
             "children": [{"name": trastero.nombre,
                           "size": 1} for trastero in Trastero.objects.all()]},
            {"name": "Proyectos",
             "children": [{"name": proyecto.nombre,
                           "size": 1} for proyecto in ProyectoProductivo.objects.all()]},
            {"name": "Habitantes en Piso",
             "children": [{"name": str(habitantepiso),
                           "size": 1} for habitantepiso in HabitantePiso.objects.all()]},
            {"name": "Personas con Trastero",
             "children": [{"name": str(personatrastero),
                           "size": 1} for personatrastero in PersonaTrastero.objects.all()]},
            {"name": "Entradas", "size": Entrada.objects.count()},
            {"name": "Salidas", "size": Salida.objects.count()}]
        }
        
    return JsonResponse(data)


def get_entradas_pie_chart_data(request):
    """
    It returns a json containing the number of 'Entrada'
    instances per subclass. It will be used to render the
    pie chart in 'Balance general' section.
    """
    data = {
        "Mantenimiento": int(sum([entrada.importe for entrada in EntradaMantenimiento.objects.all()])),
        "Alquileres": int(sum([entrada.importe for entrada in EntradaAlquiler.objects.all()])),
        "Donativos": int(sum([entrada.importe for entrada in EntradaDonativo.objects.all()])),
        "Pisos": int(sum([entrada.importe for entrada in EntradaPiso.objects.all()])),
        "Alquiler trasteros": int(sum([entrada.importe for entrada in EntradaAlquilerTrastero.objects.all()])),
        "Eventos": int(sum([entrada.importe for entrada in EntradaBeneficiosEvento.objects.all()])),
        "Proyectos Productivos": int(sum([entrada.importe for entrada in EntradaBeneficioProyectoProductivo.objects.all()])) + int(sum([entrada.importe for entrada in EntradaEspacioProyectoProductivo.objects.all()])),
        "Otras entradas": int(sum([entrada.importe for entrada in OtraEntrada.objects.all()])),
    }
    data = [{"label": k, "value": v} for k,v in data.items()]
    return JsonResponse(data, safe=False)


def get_salidas_pie_chart_data(request):
    """
    It returns a json containing the number of 'Salida'
    instances per type. It will be used to render the
    pie chart in 'Balance general' section.
    """
    data = {
        "Alquiler finca": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="alquiler_compra")])),
        "Rehabilitación": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="material_rehabilitacion")])),
        "Butano": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="butano")])),
        "Impuestos": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="impuesto")])),
        "Comisiones": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="comision")])),
        "Internet": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="internet")])),
        "Agua": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="agua")])),
        "Fiare": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="fiare")])),
        "Otras salidas": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="otra")]))
    }
    data = [{"label": k, "value": v} for k,v in data.items()]
    
    return JsonResponse(data, safe=False)


def get_balance_general_data(request):
    """
    It returns a json that contains the data to visualize
    linear graph about accounting.
    """
    entradas = [{'fecha': str(entrada.fecha), 'importe': int(entrada.importe)} for entrada in Entrada.objects.filter(fecha__gt=(datetime.now() - timedelta(days=365))).order_by('fecha')]
    salidas = [{'fecha': str(salida.fecha), 'importe': int(salida.importe)} for salida in Salida.objects.filter(fecha__gt=(datetime.now() - timedelta(days=365))).order_by('fecha')]
    data = [
        {
            'label': "Entradas",
            'dates': [entrada['fecha'] for entrada in entradas],
            'prices': [entrada['importe'] for entrada in entradas],
        },
        {
            'label': "Salidas",
            'dates': [salida['fecha'] for salida in salidas],
            'prices': [salida['importe'] for salida in salidas],
        }
    ]

    return JsonResponse(data, safe=False)
        
