from django import template
from django.forms import fields


register = template.Library()


@register.inclusion_tag("tags/form_field.html")
def form_field(field, hide_label=False, hide_help=False):
    """
    Tag for rendering uniformly form fields.
    """
    return {
        'field': field,
        'hide_label': hide_label,
        'hide_help': hide_help,
        }


@register.inclusion_tag("tags/non_field_errors.html")
def non_field_errors(form):
    """
    Tag for rendering uniformly non form field errors
    """
    return {
        'form': form,
        }


@register.filter
def to_class_name(instance):
    return instance.__class__.__name__


@register.filter
def previous_line_in_list(lines, line):
    index = lines.index(line)
    return lines[index-1]


@register.filter(name="is_date")
def is_date(value):
    return isinstance(value, fields.DateInput)


@register.filter(name="is_select")
def is_select(value):
    return isinstance(value, fields.Select)

