"""
Reporting utils for druid app.
"""
from datetime import date
from decimal import Decimal as D

from django.db.models import Q
from django.db.models import Sum
from django.db.models.functions import Coalesce

from druids import models


class CommonUtils():
    """
    Common reporting utils.
    """
    @staticmethod
    def get_percentage_value(portion, total):
        return (portion * D("100.00") / total).quantize(D("0.01"))

    @staticmethod
    def get_positive_debt_queryset(attr, qs):
        return list(
            filter(lambda x: getattr(x, attr) > D("0.00"), qs)
        )

    @staticmethod
    def get_debt_for_attr_and_queryset(attr, qs):
        return sum(map(lambda x: getattr(x, attr), qs))

    def get_debt_for_attr_and_contract_queryset(
            self, attr, contracts, exclude_negative_debt=False,
    ):
        if exclude_negative_debt:
            contracts = self.get_positive_debt_queryset(
                attr, contracts,
            )
        return self.get_debt_for_attr_and_queryset(
            attr, contracts,
        )

    def estimate_average(
            self, value, attr, population, exclude_negative_debt=False
    ):
        if exclude_negative_debt:
            population = self.get_positive_debt_queryset(attr, population)
        return (value / len(population)).quantize(D("0.01"))

    @staticmethod
    def count_population_attr_over_average(average, attr, population):
        return len(list(filter(lambda x: getattr(x, attr) >= average, population)))

    @staticmethod
    def count_population_attr_under_average(average, attr, population):
        return len(list(filter(lambda x: getattr(x, attr) < average, population)))


class Habs():
    """
    Habitante related reporting utils.
    """
    
    @staticmethod
    def get_habs():
        return models.Habitante.objects.all()

    @staticmethod
    def get_active_habs(habs):
        return habs.filter(fecha_de_salida__isnull=True)

    @staticmethod
    def get_inactive_habs(habs):
        return habs.exclude(fecha_de_salida__isnull=True)

    @staticmethod
    def get_active_habs_pre_data(active_habs):
        return active_habs.filter(
            fecha_de_entrada__lt=date(year=2016, month=1, day=1),
        )

    @staticmethod
    def get_active_habs_post_data(active_habs):
        return active_habs.filter(
            fecha_de_entrada__gte=date(year=2016, month=1, day=1),
        )


class Projects():
    """
    Projects related reporting utils.
    """
    @staticmethod
    def get_projects():
        return models.ProyectoProductivo.objects.all()

    @staticmethod
    def get_projects_with_benefits(projects):
        return projects.annotate(
            suma_aportacion_beneficios=Coalesce(
                Sum(
                    "entradas_beneficio__importe", filter=Q(
                        ~Q(
                            entradas_beneficio__registro__nombre="Caja histórico",
                        )
                    ),
                ),                
                D("0.00"),
            )
        ).filter(suma_aportacion_beneficios__gt=D("0.00"))
            
    @staticmethod
    def get_active_projects(projects):
        return projects.filter(fecha_disolucion__isnull=True)

    @staticmethod
    def get_inactive_projects(projects):
        return projects.exclude(fecha_disolucion__isnull=True)

    @staticmethod
    def get_active_projects_pre_data(active_projects):
        return active_projects.filter(
            fecha_creacion__lt=date(year=2016, month=1, day=1),
        )

    @staticmethod
    def get_active_projects_post_data(active_projects):
        return active_projects.filter(
            fecha_creacion__gte=date(year=2016, month=1, day=1),
        )

    @staticmethod
    def get_benefit_for_projects_queryset(projects):
        return sum(
            map(
                lambda x: x.entradas_beneficio.filter(
                    Q(
                        ~Q(registro__nombre="Caja histórico"),
                    ),
                ).aggregate(
                    benefits=Coalesce(Sum("importe"), D("0.00")),
                )["benefits"],
                projects,
            )
        )

    @staticmethod
    def get_project_debt_for_projects_queryset(
            projects, exclude_negative_debt=False,
    ):
        if exclude_negative_debt:
            projects = CommonUtils().get_positive_debt_queryset(
                "deuda_espacio", projects,
            )
        return CommonUtils().get_debt_for_attr_and_queryset(
            "deuda_espacio", projects,
        )

    def get_balance_for_projects_queryset(self, projects):
        return self.get_benefit_for_projects_queryset(
            projects
        ) - self.get_project_debt_for_projects_queryset(projects)


class Maintenance():
    """
    Maintenance related reporting utils.
    """
    @staticmethod
    def get_maintenance_debt_for_habs_queryset(habs, exclude_negative_debt=False):
        if exclude_negative_debt:
            habs = CommonUtils().get_positive_debt_queryset(
                "deuda_mantenimiento", habs,
            )
        return CommonUtils().get_debt_for_attr_and_queryset(
            "deuda_mantenimiento", habs,
        )
    @staticmethod
    def estimate_average(value, population, exclude_negative_debt):
        return CommonUtils().estimate_average(
            value, "deuda_mantenimiento", population, exclude_negative_debt,
        )

    @staticmethod
    def population_over_average(average, population):
        return CommonUtils().count_population_attr_over_average(
            average, "deuda_mantenimiento", population,
        )

    @staticmethod
    def population_under_average(average, population):
        return CommonUtils().count_population_attr_under_average(
            average, "deuda_mantenimiento", population,
        )


class Housing():
    """
    Housing related reporting utils.
    """
    @staticmethod
    def _get_housing_debt_for_contract_queryset(
            contracts, exclude_negative_debt=False,
    ):
        return CommonUtils().get_debt_for_attr_and_contract_queryset(
            "deuda", contracts, exclude_negative_debt,
        )

    def get_housing_debt_for_habs_queryset(
            self,  habs, exclude_negative_debt=False
    ):
        return sum(
            map(
                lambda x: self._get_housing_debt_for_contract_queryset(
                    x.habitantepiso_set.all(),
                    exclude_negative_debt,
                ),
                habs,
            )
        )

    def estimate_average(self, value, population, exclude_negative_debt):
        housing_people = list(filter(lambda x: x.tiene_pacto_de_piso, population))
        return (
            sum(
                map(
                    lambda x: self._get_housing_debt_for_contract_queryset(
                        x.habitantepiso_set.all(),
                        exclude_negative_debt,
                    ),
                    housing_people,
                )
            ) / D(len(housing_people))
        ).quantize(D("0.01"))

    def population_over_average(self, average, population):
        housing_people = list(filter(lambda x: x.tiene_pacto_de_piso, population))
        return len(
            list(
                filter(
                    lambda x: self._get_housing_debt_for_contract_queryset(
                        x.habitantepiso_set.all()
                    ) >= average,
                    housing_people,
                )
            )
        )

    def population_under_average(self, average, population):
        housing_people = list(filter(lambda x: x.tiene_pacto_de_piso, population))
        return len(
            list(
                filter(
                    lambda x: self._get_housing_debt_for_contract_queryset(
                        x.habitantepiso_set.all()
                    ) < average,
                    housing_people,
                )
            )
        )


class Storage():
    """
    Storage related reporting utils.
    """
    @staticmethod
    def _get_storage_debt_for_contract_queryset(
            contracts, exclude_negative_debt=False,
    ):
        return CommonUtils().get_debt_for_attr_and_contract_queryset(
            "deuda", contracts,
        )

    def get_storage_debt_for_habs_queryset(
            self,  habs, exclude_negative_debt=False
    ):
        return sum(
            map(
                lambda x: self._get_storage_debt_for_contract_queryset(
                    x.personatrastero_set.all(),
                    exclude_negative_debt,
                ),
                habs,
            )
        )

    def estimate_average(self, value, population, exclude_negative_debt):
        storage_people = list(filter(lambda x: x.tiene_pacto_trastero, population))
        return (
            sum(
                map(
                    lambda x: self._get_storage_debt_for_contract_queryset(
                        x.personatrastero_set.all(),
                        exclude_negative_debt,
                    ),
                    storage_people,
                )
            ) / D(len(storage_people))
        ).quantize(D("0.01"))

    def population_over_average(self, average, population):
        storage_people = list(filter(lambda x: x.tiene_pacto_trastero, population))
        return len(
            list(
                filter(
                    lambda x: self._get_storage_debt_for_contract_queryset(
                        x.personatrastero_set.all()
                    ) >= average,
                    storage_people,
                )
            )
        )

    def population_under_average(self, average, population):
        storage_people = list(filter(lambda x: x.tiene_pacto_trastero, population))
        return len(
            list(
                filter(
                    lambda x: self._get_storage_debt_for_contract_queryset(
                        x.personatrastero_set.all()
                    ) < average,
                    storage_people,
                )
            )
        )
