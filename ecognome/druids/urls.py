from django.conf.urls import re_path

from . import views

app_name = 'druids'
urlpatterns = [

    # GT economia

    ## Estadísticas

    re_path(
        r'^gt-economia/estadisticas/reportes_mensuales_por_tipo/(?P<pk>\d+)/',
        views.ReporteMensualPorTipoDetailView.as_view(),
        name="reporte-mensual-por-tipo",
    ),

    re_path(
        r'^gt-economia/estadisticas/reportes_mensuales_por_tipo/',
        views.reportes_mensuales_por_tipo,
        name="reportes-mensuales-por-tipo",
    ),
    
    ## Cuotas
    re_path(r'^gt-economia/cuotas/mantenimiento/crear/',
        views.CuotaMantenimientoCreateView.as_view(),
        name="crear-mantenimiento"
        ),
    re_path(
        r'^gt-economia/cuotas/mantenimiento/(?P<pk>\d+)/',
        views.CuotaMantenimientoUpdateView.as_view(),
        name="desactivar-mantenimiento",
    ),
    re_path(r'^gt-economia/cuotas/mantenimiento/',
        views.CuotaMantenimientoListView.as_view(),
        name="lista-mantenimiento"
        ),
    re_path(r'^gt-economia/cuotas/piso/crear/',
        views.CuotaPisoCreateView.as_view(),
        name="crear-cuota-piso"
        ),
    re_path(r'^gt-economia/cuotas/piso/',
        views.CuotaPisoListView.as_view(),
        name="lista-cuotas-piso"
        ),
    
    ## Espacios
    re_path(r'^gt-economia/espacios/pisos/crear/',
        views.PisoCreateView.as_view(),
        name="crear-piso"
        ),
    re_path(r'^gt-economia/espacios/pisos/(?P<pk>\d+)/',
        views.PisoUpdateView.as_view(),
        name="editar-piso"
        ),
    re_path(r'^gt-economia/espacios/pisos/',
        views.PisoListView.as_view(),
        name="lista-pisos"
        ),
    re_path(r'^gt-economia/espacios/trasteros/crear/',
        views.TrasteroCreateView.as_view(),
        name="crear-trastero"
        ),
    re_path(r'^gt-economia/espacios/trasteros/(?P<pk>\d+)/',
        views.TrasteroUpdateView.as_view(),
        name="editar-trastero"
        ),
    re_path(r'^gt-economia/espacios/trasteros/',
        views.TrasteroListView.as_view(),
        name="lista-trasteros"
        ),

    ## Cuadernos Contables
    re_path(r'^gt-economia/cuadernos/entradas/(?P<tipo_entrada>\w+)',
        views.cuaderno_contable_entradas,
        name="cuaderno_contable_entradas",
        ),
    re_path(r'^gt-economia/cuadernos/entradas',
        views.cuaderno_contable_entradas,
        name="cuaderno_contable_entradas",
        ),
    re_path(r'^gt-economia/cuadernos/salidas/(?P<tipo_salida>\w+)',
        views.cuaderno_contable_salidas,
        name="cuaderno_contable_salidas",
        ),
    re_path(r'^gt-economia/cuadernos/salidas',
        views.cuaderno_contable_salidas,
        name="cuaderno_contable_salidas",
        ),
    
    ## Registros
    re_path(r'^gt-economia/registros/metalico/crear/',
        views.MetalicoCreateView.as_view(),
        name="crear-metalico"
        ),
    re_path(r'^gt-economia/registros/metalico/',
        views.MetalicoListView.as_view(),
        name="lista-metalicos"
        ),
    re_path(r'^gt-economia/registros/banco/crear/',
        views.BancoCreateView.as_view(),
        name="crear-banco"
        ),
    re_path(r'^gt-economia/registros/banco/',
        views.BancoListView.as_view(),
        name="lista-bancos"
        ),
    re_path(r'^gt-economia/registros/electronico/crear/',
        views.PaypalCreateView.as_view(),
        name="crear-paypal"
        ),
    re_path(r'^gt-economia/registros/electronico/',
        views.PaypalListView.as_view(),
        name="lista-paypals"
        ),
    re_path(r'^gt-economia/registros/extracto/(?P<registro_id>\d+)/',
        views.extracto_registro,
        name="extracto-registro"
        ),
    re_path(r'^gt-economia/registros/extracto/rectificar_salida/(?P<salida_id>\d+)/',
        views.rectificar_salida,
        name="rectificar-salida"
        ),
    re_path(r'^gt-economia/registros/extracto/rectificar_entrada/(?P<entrada_id>\d+)/',
        views.rectificar_entrada,
        name="rectificar-entrada"
        ),
    
    ## Proyectos
    re_path(r'^gt-economia/proyectos/crear/',
        views.ProyectoProductivoCreateView.as_view(),
        name="crear-proyecto"
        ),
    re_path(r'^gt-economia/proyectos/(?P<pk>\d+)/',
        views.ProyectoProductivoUpdateView.as_view(),
        name="editar-proyecto"
        ),
    re_path(r'^gt-economia/proyectos/',
        views.ProyectoProductivoListView.as_view(),
        name="lista-proyectos"
        ),

    ## Personas
    re_path(r'^gt-economia/personas/habitantes/crear/',
        views.HabitanteCreateView.as_view(),
        name="crear-habitante"
        ),
    re_path(r'^gt-economia/personas/habitantes/(?P<pk>\d+)/',
        views.HabitanteUpdateView.as_view(),
        name="editar-habitante"
        ),
    re_path(r'^gt-economia/personas/habitantes/',
        views.HabitanteListView.as_view(),
        name="lista-habitantes"
        ),
    re_path(r'^gt-economia/personas/invitadas/crear/',
        views.InvitadaCreateView.as_view(),
        name="crear-invitada"
        ),
    re_path(r'^gt-economia/personas/invitadas/(?P<pk>\d+)/',
        views.InvitadaUpdateView.as_view(),
        name="editar-invitada"
        ),
    re_path(r'^gt-economia/personas/invitadas/',
        views.InvitadaListView.as_view(),
        name="lista-invitadas"
        ),
    re_path(r'^gt-economia/personas/huespedes/crear/',
        views.HuespedCreateView.as_view(),
        name="crear-huesped"
        ),
    re_path(r'^gt-economia/personas/huespedes/(?P<pk>\d+)/',
        views.HuespedUpdateView.as_view(),
        name="editar-huesped"
        ),
    re_path(r'^gt-economia/personas/huespedes/',
        views.HuespedListView.as_view(),
        name="lista-huespedes"
        ),

    ## Pactos
    re_path(r'^gt-economia/pactos/habitantepiso/crear/',
        views.HabitantePisoCreateView.as_view(),
        name="crear-habitantepiso"
        ),
    re_path(r'^gt-economia/pactos/habitantepiso/(?P<pk>\d+)/',
        views.HabitantePisoUpdateView.as_view(),
        name="editar-habitantepiso"
        ),
    re_path(r'^gt-economia/pactos/habitantepiso/',
        views.HabitantePisoListView.as_view(),
        name="lista-habitantepiso"
        ),
    re_path(r'^gt-economia/pactos/personatrastero/crear/',
        views.PersonaTrasteroCreateView.as_view(),
        name="crear-personatrastero"
        ),
    re_path(r'^gt-economia/pactos/personatrastero/(?P<pk>\d+)/',
        views.PersonaTrasteroUpdateView.as_view(),
        name="editar-personatrastero"
        ),
    re_path(r'^gt-economia/pactos/personatrastero/',
        views.PersonaTrasteroListView.as_view(),
        name="lista-personatrastero"
        ),

    ## Movimientos
    re_path(r'^gt-economia/movimientos/salidas/crear/',
        views.insertar_salidas,
        name="crear-salida"
        ),
    re_path(r'^gt-economia/movimientos/entradas/otras/crear/',
        views.insertar_entradas_otras,
        name="crear-otras-entradas"
        ),
    re_path(r'^gt-economia/movimientos/entradas/alquileres/crear/',
        views.insertar_entradas_alquileres,
        name="crear-alquileres-entradas"
        ),
    re_path(r'^gt-economia/movimientos/entradas/proyectos/crear/',
        views.insertar_entradas_proyectos,
        name="crear-proyectos-entradas"
        ),

    ## Balances
    re_path(r'^gt-economia/balances/crear/',
        views.BalanceCreateView.as_view(),
        name="crear-balance"
        ),
    re_path(r'^gt-economia/balances/(?P<pk>\d+)/',
        views.BalanceDetailView.as_view(),
        name="ver-balance"
        ),
    re_path(r'^gt-economia/balances/',
        views.BalanceListView.as_view(),
        name="lista-balances"
        ),

    ## Export CSV

    re_path(r'^gt-economia/csv/entradas/$',
        views.exportar_entradas_csv,
        name="exportar_entradas_csv",
        ),
    re_path(r'^gt-economia/csv/salidas/$',
        views.exportar_salidas_csv,
        name="exportar_salidas_csv",
        ),
    re_path(r'^gt-economia/csv/pisos/$',
        views.exportar_pisos_csv,
        name="exportar_pisos_csv",
        ),
    re_path(r'^gt-economia/csv/trasteros/$',
        views.exportar_trasteros_csv,
        name="exportar_trasteros_csv",
        ),
    re_path(r'^gt-economia/csv/cuotas_piso/$',
        views.exportar_cuotas_piso_csv,
        name="exportar_cuotas_piso_csv",
        ),
    re_path(r'^gt-economia/csv/cuotas_mantenimiento/$',
        views.exportar_cuotas_mantenimiento_csv,
        name="exportar_cuotas_mantenimiento_csv",
        ),
    re_path(r'^gt-economia/csv/habitante_piso/$',
        views.exportar_habitante_piso_csv,
        name="exportar_habitante_piso_csv",
        ),
    re_path(r'^gt-economia/csv/persona_trastero/$',
        views.exportar_persona_trastero_csv,
        name="exportar_persona_trastero_csv",
        ),
    re_path(r'^gt-economia/csv/personas/$',
        views.exportar_personas_csv,
        name="exportar_personas_csv",
        ),
    re_path(r'^gt-economia/csv/registros/$',
        views.exportar_registros_csv,
        name="exportar_registros_csv",
        ),
    re_path(r'^gt-economia/csv/proyectos/$',
        views.exportar_proyectos_csv,
        name="exportar_proyectos_csv",
        ),
    
    ## Dashboard
    re_path(r'^gt-economia/', views.gt_dashboard, name='gt-dashboard'),

    # Public dashboard
    ## API
    re_path(r'^api/cloud-data/', views.get_dashboard_data, name='api-cloud-data'),
    re_path(r'^api/entradas-pie/', views.get_entradas_pie_chart_data, name='api-entradas-pie'),
    re_path(r'^api/salidas-pie/', views.get_salidas_pie_chart_data, name='api-salidas-pie'),
    re_path(r'^api/balance-line-graph/', views.get_balance_general_data, name='api-balance-line-graph'),

    ## Situaciones personales
    re_path(
        r'^situaciones-personales/(?P<habitante_id>\d+)/',
        views.SituacionesPersonalesView.as_view(),
        name='situaciones-personales'
    ),
    re_path(
        r'^situaciones-personales/',
        views.SituacionesPersonalesView.as_view(),
        name='situaciones-personales'
    ),

    ## Proyectos productivos
    re_path(
        r'^proyectos-productivos/(?P<proyecto_id>\d+)/',
        views.ProyectosProductivosView.as_view(),
        name='proyectos-productivos'
    ),
    re_path(
        r'^proyectos-productivos/',
        views.ProyectosProductivosView.as_view(),
        name='proyectos-productivos'
    ),
    
    ## Balance general
    re_path(r'^balance-general/', views.balance_general, name='balance-general'),
    
    ## Index
    re_path(r'^$', views.HomeView.as_view(), name='index'),

]

