"""
Module that generates console report about productive projects.
"""
from django.core.management.base import BaseCommand

from druids.reporting import Projects


class Command(BaseCommand):
    """
    Generate report for internal debt explanation.
    """

    def handle(self, *args, **options):
        projects = Projects().get_projects()
        productive_projects = Projects().get_projects_with_benefits(projects)
        active_productive_projects = Projects().get_active_projects(
            productive_projects
        )

        self.stdout.write(
            "Reporte  de proyectos que reportan beneficios"
        )
        for project in active_productive_projects:

            total_productive_debt = project.deuda_espacio
            total_benefit = project.aportacion_beneficios_sin_historico
            
            self.stdout.write("")
            self.stdout.write(project.nombre)
            self.stdout.write("")
            self.stdout.write(
                "* Total deuda: {}€".format(
                    total_productive_debt,
                )
            )
            self.stdout.write(
                "* Total beneficios {}€".format(
                    total_benefit,
                )
            )
