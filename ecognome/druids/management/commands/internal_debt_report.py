"""
Module that generates console report about internal debt.
"""
from django.core.management.base import BaseCommand

from druids.reporting import CommonUtils
from druids.reporting import Habs
from druids.reporting import Maintenance
from druids.reporting import Housing
from druids.reporting import Storage
from druids.reporting import Projects


class Command(BaseCommand):
    """
    Generate report for internal debt explanation.
    """

    @staticmethod
    def _get_options_mapper():
        return {
            "mantenimiento": {
                "debt": Maintenance().get_maintenance_debt_for_habs_queryset,
                "average": Maintenance().estimate_average,
                "over_average": Maintenance().population_over_average,
                "under_average": Maintenance().population_under_average,
            },
            "pisos": {
                "debt": Housing().get_housing_debt_for_habs_queryset,
                "average": Housing().estimate_average,
                "over_average": Housing().population_over_average,
                "under_average": Housing().population_under_average,
            },
            "trastero": {
                "debt": Storage().get_storage_debt_for_habs_queryset,
                "average": Storage().estimate_average,
                "over_average": Storage().population_over_average,
                "under_average": Storage().population_under_average,
            },
        }

    def handle(self, *args, **options):
        habs = Habs().get_habs()
        active_habs = Habs().get_active_habs(habs)
        inactive_habs = Habs().get_inactive_habs(habs)
        exclude_negative_debt = True

        for key, handler in self._get_options_mapper().items():
            total_debt = handler["debt"](
                habs, exclude_negative_debt,
            )
            inactive_debt = handler["debt"](
                inactive_habs, exclude_negative_debt,
            )
            active_debt = handler["debt"](
                active_habs, exclude_negative_debt,
            )
            total_average = handler["average"](
                total_debt,
                habs,
                exclude_negative_debt,
            )
            inactive_average = handler["average"](
                inactive_debt,
                inactive_habs,
                exclude_negative_debt,
            )
            active_average = handler["average"](
                active_debt,
                active_habs,
                exclude_negative_debt,
            )

            subtitle = "teniendo en cuenta la deuda negativa" if \
                       not exclude_negative_debt \
                       else "excluyendo la deuda negativa"

            self.stdout.write(
                "Reporte de deuda interna de {} {}".format(key, subtitle)
            )
            self.stdout.write("")
            self.stdout.write(
                "Total deuda: {}€ ({}€/hab.)".format(
                    total_debt,
                    total_average ,
                )
            )

            self.stdout.write(
                "Habs. por encima o igual a la media: {}".format(
                    handler["over_average"](
                        total_average,
                        habs,
                    ),
                )
            )

            self.stdout.write(
                "Habs. por debajo de la media: {}".format(
                    handler["under_average"](
                        total_average,
                        habs,
                    ),
                )
            )

            self.stdout.write(
                "Total deuda inactiva: {}€ ({}% sobre total) "
                "({}€ / hab.)".format(
                    inactive_debt,
                    CommonUtils().get_percentage_value(
                        inactive_debt,
                        total_debt,
                    ),
                    inactive_average,
                )
            )
            self.stdout.write(
                "Habs. por encima o igual a la media: {}".format(
                    handler["over_average"](
                        inactive_average,
                        inactive_habs,
                    ),
                )
            )

            self.stdout.write(
                "Habs. por debajo de la media: {}".format(
                    handler["under_average"](
                        inactive_average,
                        inactive_habs,
                    ),
                )
            )
            self.stdout.write(
                "Total deuda activa: {}€ ({}% sobre total) "
                "({}€ / hab.)".format(
                    active_debt,
                    CommonUtils().get_percentage_value(
                        active_debt,
                        total_debt,
                    ),
                    active_average,
                )
            )
            self.stdout.write(
                "Habs. por encima o igual a la media: {}".format(
                    handler["over_average"](
                        active_average,
                        active_habs,
                    ),
                )
            )

            self.stdout.write(
                "Habs. por debajo de la media: {}".format(
                    handler["under_average"](
                        active_average,
                        active_habs,
                    ),
                )
            )

            self.stdout.write("")
            self.stdout.write("")
