"""
This module present one way to migrate data from project
version 1.x.x to version 2.x.x
"""
from datetime import datetime
from decimal import Decimal as D
import csv
import os

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError

from druids import models


class Command(BaseCommand):
    """
    Importer from data version 1.x.x to version 2.x.x
    """
    help = "Parse exported models via csv and migrate them into new schema"

    @staticmethod
    def _parsedate(datestring):
        if datestring == "None":
            return None
        return datetime.strptime(datestring, "%Y-%m-%d").date()

    @staticmethod
    def _get_ordered_by_dependency_filename_list():
        return [
            # No Foreign Keys
            "cuotas_mantenimiento.csv",
            "cuotas_piso.csv",
            "proyectos.csv",
            "registros.csv",
            "trasteros.csv",
            # Foreign Keys
            "pisos.csv",
            "personas.csv",
            "habitante_piso.csv",
            "persona_trastero.csv",
            "salidas.csv",
            "entradas.csv",
        ]

    def _get_entity_order_handler_mapper(self):
        return (
            (0, self._import_cuota_mantenimiento),
            (1, self._import_cuota_piso),
            (2, self._import_proyectos),
            (3, self._import_registros),
            (4, self._import_trasteros),
            (5, self._import_pisos),
            (6, self._import_personas),
            (7, self._import_habitante_piso),
            (8, self._import_persona_trastero),
            (9, self._import_salidas),
            (10, self._import_entradas),
        )
    
    @staticmethod
    def _open_csv_file(filename):
        """
        Gather data from csv file.
        """
        path = os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(__file__)
                    )
                )
            ),
            "importer_data",
            filename
        )
        handler = open(path)
        data = csv.DictReader(handler)
        return data, handler

    def _import_cuota_mantenimiento(self, data):

        for row in data:
            models.CuotaMantenimiento.objects.update_or_create(
                pk=int(row["id"]),
                importe=D(row["importe"]),
                descripcion=row["descripcion"],
                tipo=row["tipo"],
                fecha_activacion=self._parsedate("2014-07-01"),
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Cuotas de mantenimiento importadas"
            )
        )

    def _import_cuota_piso(self, data):

        for row in data:
            models.CuotaPiso.objects.update_or_create(
                pk=int(row["id"]),
                importe=D(row["importe"]),
                descripcion=row["descripcion"],
                cuota_entrada=D("560.00"),
                cuota_onduline=D("175.00"),
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Cuotas de piso importadas"
            )
        )

    def _import_proyectos(self, data):

        for row in data:
            models.ProyectoProductivo.objects.update_or_create(
                pk=int(row["id"]),
                nombre=row["nombre"],
                cuota_espacio=D(row["cuota_espacio"]),
                fecha_creacion=self._parsedate(row["fecha_creacion"]),
                fecha_disolucion=self._parsedate(row["fecha_disolucion"]),
                comentarios=row["comentarios"],
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Proyectos importados"
            )
        )

    def _import_registros(self, data):

        for row in data:
            if row["tipo"] == "metalico":
                models.Metalico.objects.update_or_create(
                    pk=int(row["id"]),
                    nombre=row["nombre"],
                )
            if row["tipo"] == "banco":
                models.Banco.objects.update_or_create(
                    pk=int(row["id"]),
                    nombre=row["nombre"],
                )
            if row["tipo"] == "electronico":
                models.PayPal.objects.update_or_create(
                    pk=int(row["id"]),
                    nombre=row["nombre"],
                )
        self.stdout.write(
            self.style.SUCCESS(
                "Registros importados"
            )
        )

    def _import_trasteros(self, data):

        for row in data:
            models.Trastero.objects.update_or_create(
                pk=int(row["id"]),
                nombre=row["nombre"],
                cuota_trastero=D(row["cuota_trastero"]),
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Trasteros importados"
            )
        )

    def _import_pisos(self, data):

        for row in data:
            models.Piso.objects.update_or_create(
                pk=int(row["id"]),
                nombre=row["nombre"],
                valor_total=D(row["valor_total"]),
                cuota_piso_id=int(row["id_cuota_piso"]),
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Pisos importados"
            )
        )

    def _import_personas(self, data):

        for row in data:
            if row["id_como_habitante"] != "---":
                hab, _ = models.Habitante.objects.update_or_create(
                    pk=int(row["id_como_habitante"]),
                    nombre=row["nombre"],
                    fecha_de_entrada=self._parsedate(row["fecha_de_entrada"]),
                    fecha_de_salida=self._parsedate(row["fecha_de_salida"]),
                )
                hab.cuotas_mantenimiento_adheridas.clear()
                hab.cuotas_mantenimiento_adheridas.add(
                    models.CuotaMantenimiento.objects.get(
                        pk=int(row["id_cuota_mantenimiento"]),
                    )
                )
            else:
                inv, _ = models.Invitada.objects.update_or_create(
                    pk=int(row["id_como_persona"]),
                    nombre=row["nombre"],
                    fecha_de_entrada=self._parsedate(row["fecha_de_entrada"]),
                    fecha_de_salida=self._parsedate(row["fecha_de_salida"]),
                )
                inv.cuotas_mantenimiento_adheridas.clear()
                inv.cuotas_mantenimiento_adheridas.add(
                    models.CuotaMantenimiento.objects.get(
                        pk=int(row["id_cuota_mantenimiento"]),
                    )
                )
        self.stdout.write(
            self.style.SUCCESS(
                "Personas importadas"
            )
        )

    def _import_habitante_piso(self, data):

        for row in data:
            models.HabitantePiso.objects.update_or_create(
                pk=int(row["id"]),
                habitante_id=int(row["id_habitante"]),
                piso_id=int(row["id_piso"]),
                fecha_entrada=self._parsedate(row["fecha_entrada"]),
                fecha_salida=self._parsedate(row["fecha_salida"]),
                comentarios=row["comentarios"],
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Habitantas de piso importadas"
            )
        )

    def _import_persona_trastero(self, data):

        for row in data:
            models.PersonaTrastero.objects.update_or_create(
                pk=int(row["id"]),
                persona_id=int(row["id_persona"]),
                trastero_id=int(row["id_trastero"]),
                fecha_entrada=self._parsedate(row["fecha_entrada"]),
                fecha_salida=self._parsedate(row["fecha_salida"]),
                comentarios=row["comentarios"],
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Personas de trasteros importadas"
            )
        )

    def _import_salidas(self, data):

        for row in data:
            models.Salida.objects.update_or_create(
                pk=int(row["id"]),
                fecha=self._parsedate(row["fecha"]),
                tipo_salida=row["tipo"],
                registro_id=int(row["id_registro"]),
                comentarios=row["comentarios"],
                usuaria_economia=User.objects.get(
                    username=row["usuaria_economia"]
                ),
                importe=D(row["importe"]),
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Salidas importadas"
            )
        )

    @staticmethod
    def _get_model_for_entrada(tipo):
        mapper = {
            "mantenimiento": models.EntradaMantenimiento,
            "piso": models.EntradaPiso,
            "alquiler": models.EntradaAlquiler,
            "donativo": models.EntradaDonativo,
            "evento": models.EntradaBeneficiosEvento,
            "espacio_pp": models.EntradaEspacioProyectoProductivo,
            "beneficio_pp": models.EntradaBeneficioProyectoProductivo,
            "trastero": models.EntradaAlquilerTrastero,
            "otra": models.OtraEntrada,
            "transferencia": models.EntradaTransferencia,
        }
        return mapper[tipo]

    @staticmethod
    def _build_extra_kwargs_for_entrada(row):
        mapper = {
            "con persona": {"persona_id": row["nombre_relacion"]},
            "con habitante_piso": {"habitante_piso_id": row["nombre_relacion"]},
            "con proyecto_productivo": {
                "proyecto_productivo_id": row["nombre_relacion"]
            },
            "con persona_trastero": {"persona_trastero_id": row["nombre_relacion"]},
            "---": {},
        }
        return mapper[row["id_relacion"]]

    def _import_entradas(self, data):
        for row in data:
            kwargs = {
                "pk": int(row["id"]),
                "fecha": self._parsedate(row["fecha"]),
                "tipo": row["tipo"],
                "importe": D(row["importe"]),
                "registro_id": int(row["id_registro"]),
                "usuaria_economia": User.objects.get(
                    username=row["usuaria_economia"]
                ),
                "comentarios": row["comentarios"],
            }
            extra_data = self._build_extra_kwargs_for_entrada(row)
            kwargs.update(extra_data)
            self._get_model_for_entrada(row["tipo"]).objects.update_or_create(
                **kwargs
            )

        self.stdout.write(
            self.style.SUCCESS(
                "Entradas importadas"
            )
        )

    def handle(self, *args, **options):
        for idx, handler in self._get_entity_order_handler_mapper():
            data, iohandler = self._open_csv_file(
                self._get_ordered_by_dependency_filename_list()[idx]
            )
            handler(data)
            iohandler.close()
