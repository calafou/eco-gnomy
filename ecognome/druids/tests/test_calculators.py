"""
Test cases for calculators.
"""
from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal as D
import unittest

from druids.calculators import HousingCalculator
from druids.calculators import MaintenanceCalculator
from druids.calculators import StorageCalculator


class MaintenanceCalculatorTestCase(unittest.TestCase):
    """
    Test Case for MaintenanceCalculator class.
    """
    def setUp(self):
        self.cutoff = date(2018, 12, 9)
        self.cuton = date(2016, 12, 3)

    def test_estimated_maintenance_shares_with_left_date(self):
        fees = self._fees_generator()

        self.assertEqual(
            len(
                MaintenanceCalculator().estimated_maintenance_shares(
                    fees, self.cuton, self.cutoff,
                )
            ),
            4,
        )

    def test_estimated_maintenance_shares_with_no_left_date(self):
        fees = self._fees_generator()

        self.assertEqual(
            MaintenanceCalculator().estimated_maintenance_shares(
                fees,
                self.cuton,
            ),
            MaintenanceCalculator().estimated_maintenance_shares(
                fees,
                self.cuton,
                date.today(),
            ),
        )

    @staticmethod
    def _fees_generator(open_ending=False):
        fees = list()
        for i in range(1, 13, 3):
            ending_date = date(
                year=2017, month=i, day=1) + \
                relativedelta(months=3) - relativedelta(days=1)
            fees.append(
                dict(
                    code=i,
                    importe=D("10.00"),
                    fecha_activacion=date(year=2017, month=i, day=1),
                    fecha_desactivacion=ending_date,
                )
            )
        if open_ending:
            fees.append(
                dict(
                    code=13,
                    importe=D("10.00"),
                    fecha_activacion=date(year=2018, month=1, day=1),
                    fecha_desactivacion=None,
                )
            )
        return fees

    @staticmethod
    def _entries_generator(debt=True):
        entries = list()
        value = D("10.00") if not debt else D("5.00")
        for code in (1, 4, 7, 10):
            for i in range(3):
                entries.append(
                    dict(
                        code=code,
                        importe=value,
                        fecha=date(year=2017, month=code+i, day=1),
                    )
                )
        return entries

    def test_payed_maintenance_shares(self):
        zero_entries = zero_fees = list()
        entries = self._entries_generator(False)
        debt_entries = self._entries_generator(debt=True)
        fees = self._fees_generator()
        self.assertEqual(
            0,
            MaintenanceCalculator().payed_maintenance_shares(
                entries, zero_fees, self.cuton, self.cutoff,
            ),
        )
        self.assertEqual(
            0,
            MaintenanceCalculator().payed_maintenance_shares(
                zero_entries, fees, self.cuton, self.cutoff,
            ),
        )
        self.assertEqual(
            6,
            MaintenanceCalculator().payed_maintenance_shares(
                debt_entries, fees, self.cuton, self.cutoff,
            ),
        )
        self.assertEqual(
            8,
            MaintenanceCalculator().payed_maintenance_shares(
                entries, fees, self.cuton, self.cutoff,
            ),
        )

    def test_maintenance_debt(self):
        entries = self._entries_generator(False)
        debt_entries = self._entries_generator(debt=True)
        fees = self._fees_generator()
        self.assertEqual(
            D("-40.00"),
            MaintenanceCalculator().maintenance_debt(
                entries,
                fees,
                self.cuton,
                self.cutoff,
            ),
        )
        self.assertEqual(
            D("20.00"),
            MaintenanceCalculator().maintenance_debt(
                debt_entries,
                fees,
                self.cuton,
                self.cutoff,
            ),
        )

    def test_maintenance_debt_with_open_fees(self):
        entries = self._entries_generator(debt=False)
        fees = self._fees_generator(open_ending=True)
        open_fee = fees[4]
        delta = relativedelta(
            date.today(),
            max(self.cuton, open_fee["fecha_activacion"]),
        )
        open_fee_debt = open_fee["importe"] * (
            delta.months + delta.years * 12
        )
        estimated_debt = open_fee_debt
        for fee in fees[:4]:
            delta = relativedelta(
                min(self.cutoff, fee["fecha_desactivacion"]),
                max(self.cuton, fee["fecha_activacion"]),
            )
            estimated_debt += fee["importe"] * (delta.months + delta.years * 12)
        estimated_debt -= sum(entry["importe"] for entry in entries)
        self.assertEqual(
            estimated_debt,
            MaintenanceCalculator().maintenance_debt(
                entries,
                fees,
                self.cuton,
                self.cutoff,
            ),
        )


class HousingCalculatorTestCase(unittest.TestCase):
    """
    Test Case for HousingCalculator class.
    """
    def setUp(self):
        self.cuton = date(2017, 12, 1)
        self.cutoff = date(2018, 12, 9)
        self.entries = []
        self.debt_entries = [{"importe": D("20.00")}]
        self.fee = {
            "importe": D("100.00"),
            "cuota_entrada": D("100.00"),
            "cuota_onduline": D("100.00"),
        }
        for _ in range(10):
            self.entries.append({"importe": D("100.00")})

    def test_has_entrance_share(self):
        self.assertTrue(
            HousingCalculator().has_entrance_share(self.entries, self.fee)
        )
        self.assertFalse(
            HousingCalculator().has_entrance_share(self.debt_entries, self.fee)
        )

    def test_has_onduline_share(self):
        self.assertTrue(
            HousingCalculator().has_onduline_share(self.entries, self.fee)
        )
        self.assertFalse(
            HousingCalculator().has_onduline_share(self.debt_entries, self.fee)
        )

    def test_zero_payed_housing_shares(self):
        self.assertEqual(
            0,
            HousingCalculator().payed_housing_shares(self.debt_entries, self.fee)
        )

    def test_payed_housing_shares(self):
        self.assertEqual(
            8,
            HousingCalculator().payed_housing_shares(self.entries, self.fee),
        )

    def test_estimated_housing_shares(self):
        started = date(2017, 12, 1)
        concluded = date(2018, 3, 15)
        self.assertEqual(
            3,
            HousingCalculator().estimated_housing_shares(started, concluded),
        )

    def test_housing_debt(self):
        self.assertEqual(
            D("400.00"),
            HousingCalculator().housing_debt(
                self.entries, self.fee, self.cuton, self.cutoff,
            ),
        )

    def test_percentage_paid(self):
        self.assertEqual(
            10,
            HousingCalculator().percentage_paid(self.entries, 10000),
        )


class StorageCalculatorTestCase(unittest.TestCase):
    """
    Test Case for StorageCalculator class.
    """
    def setUp(self):
        self.fee = D("10.00")
        self.entries = []
        for _ in range(10):
            self.entries.append({"importe": D("10.00")})

    def test_payed_storage_shares(self):
        self.assertEqual(
            10,
            StorageCalculator().payed_storage_shares(self.entries, self.fee),
        )

    def test_storage_debt(self):
        cuton = date(2018, 1, 1)
        cutoff = date(2018, 12, 1)
        self.assertEqual(
            D("10.00"),
            StorageCalculator().storage_debt(
                self.entries, self.fee, cuton, cutoff
            ),
        )
