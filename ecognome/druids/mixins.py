"""
Mixins module for druids app.
"""
class TitleMixin():
    """
    Gather title in context.
    """
    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        data["title"] = self.title
        return data


class BreadCrumbsMixin():
    """
    Gather breadcrumbsin context.
    """
    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        data["breadcrumbs"] = self.raw_breadcrumbs
        return data

