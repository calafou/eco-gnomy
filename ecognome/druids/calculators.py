"""
Module with helper classes for operations
around movements.
"""
from datetime import date
from decimal import Decimal as D
from dateutil.relativedelta import relativedelta


class MaintenanceCalculator():
    """
    Calculator for maintenance movements.
    """ 
    def payed_maintenance_shares(self, entries, fees, cuton, cutoff=None):
        """
        Calculate how many maintenance shares have been payed 
        given a set of maintenance entries and a set of
        maintenance fees.
        """
        if len(entries) == 0 or len(fees) == 0:
            return 0

        estimated_shares = self.estimated_maintenance_shares(
            fees, cuton, cutoff,
        )
        paid_amount = sum([entry["importe"] for entry in entries])
        payed_shares = 0
        evaluating_share = None
        for share in estimated_shares:
            evaluating_share = share
            paid_amount -= share["importe"] * share["numero"]
            payed_shares += share["numero"]
            if paid_amount < D("0.00"):
                payed_shares -= abs(paid_amount / share["importe"])
                break

        return int(payed_shares)

    def estimated_maintenance_shares(self, fees, cuton, cutoff=None):
        """
        Calculate how many maintenance shares are supposed
        to be accumulated in a range of time.
        """
        if cutoff is None:
            cutoff = date.today()
        estimated_shares = []
        for fee in fees:
            fecha_desactivacion = min(
                fee["fecha_desactivacion"],
                cutoff,
            ) if fee["fecha_desactivacion"] is not None else cutoff
            fecha_activacion = max(fee["fecha_activacion"], cuton)
            delta = relativedelta(fecha_desactivacion, fecha_activacion)
            estimated_shares.append(
                {
                    "periodo": "{}--{}".format(
                        fee["fecha_activacion"],
                        fecha_desactivacion,
                    ),
                    "numero": int(delta.months + delta.years * 12),
                    "importe": fee["importe"],
                }
            )
        return estimated_shares

    def maintenance_debt(self, entries, fees, cuton, cutoff=None):
        """
        Calculate how many debt there is given a set of maintenance
        entries and a set of maintenance fees.
        """
        if cutoff is None:
            cutoff = date.today()
        paid_amount = sum([entry["importe"] for entry in entries])
        pay_amount = D("0.00")
        for fee in fees:
            fecha_desactivacion = min(
                fee["fecha_desactivacion"],
                cutoff,
            ) if fee["fecha_desactivacion"] is not None else cutoff
            fecha_activacion = max(fee["fecha_activacion"], cuton)
            delta = relativedelta(fecha_desactivacion, fecha_activacion)
            pay_amount += (delta.months + delta.years * 12) * fee["importe"]
        return pay_amount - paid_amount


class HousingCalculator():
    """
    Calculator for Living Houses movements.
    """

    @staticmethod
    def has_entrance_share(entries, fee):
        """
        Given a set of entries and a housing fee establish
        if the entrance share has been covered.
        """
        return fee["cuota_entrada"] <= sum(
            [entry["importe"] for entry in entries]
        )

    @staticmethod
    def has_onduline_share(entries, fee):
        """
        Given a set of entries and a housing fee establish
        if the onduline share has been covered.
        """
        return fee["cuota_onduline"] <= sum(
            [entry["importe"] for entry in entries]
        ) - fee["cuota_entrada"]

    def payed_housing_shares(self, entries, fee):
        """
        Given a set of entries and a housing fee establish
        how many shares have been covered.
        """
        if not (
            self.has_entrance_share(entries, fee) \
                and self.has_onduline_share(entries, fee)
        ):
            return 0

        subtotal = sum(
            [entry["importe"] for entry in entries]
        ) - fee["cuota_onduline"] - fee["cuota_entrada"]

        return int(subtotal / fee["importe"])

    @staticmethod
    def estimated_housing_shares(agreement_started, agreement_concluded=None):
        """
        Calculate how many shares are supposed to be accumulated
        given a range of time.
        """
        delta = relativedelta(
            agreement_concluded or date.today(),
            agreement_started,
        )
        return int(delta.months + delta.years * 12)

    @staticmethod
    def housing_debt(entries, fee, cuton, cutoff=None):
        """
        Calculate how many housing debt has been accumulated.
        """
        if cutoff is None:
            cutoff = date.today()
        paid_amount = sum(
            [entry["importe"] for entry in entries]
        )
        delta = relativedelta(cutoff, cuton)
        pay_amount = (delta.months + delta.years * 12) * fee["importe"] + \
                     fee["cuota_onduline"] + fee["cuota_entrada"]
        return pay_amount - paid_amount

    @staticmethod
    def percentage_paid(entries, value):
        """
        Calculate hoy many percentage is already paid.
        """
        return int(
            sum([entry["importe"] for entry in entries]) * 100 / value
        )


class StorageCalculator():
    """
    Calculator for storage movements.
    """
    @staticmethod
    def payed_storage_shares(entries, fee):
        """
        Calculate accumulated storage shares.
        """
        return int(
            sum(
                [entry["importe"] for entry in entries]
            ) / fee
        )

    @staticmethod
    def storage_debt(entries, fee, cuton, cutoff=None):
        """
        Calculate accumulated storage debt.
        """
        if cutoff is None:
            cutoff = date.today()
        paid_amount = sum([entry["importe"] for entry in entries])
        delta = relativedelta(cutoff, cuton)
        pay_amount = (delta.months + delta.years * 12) * fee

        return pay_amount - paid_amount


class ProjectCalculator():
    """
    Calculator for project movements.
    """
    @staticmethod
    def project_space_debt(entries, fee, cuton, cutoff=None):
        """
        Calculate accumulated space debt.
        """
        if cutoff is None:
            cutoff = date.today()
        paid_amount = sum([entry["importe"] for entry in entries])
        delta = relativedelta(cutoff, cuton)
        pay_amount = (delta.months + delta.years * 12) * fee
        return pay_amount - paid_amount
