// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

$.ajax({
    url: "/api/charts/balance-flow/"
}).then(function(data) {
    renderChartData(data);
});

function renderChartData(data) {

    var labels = data.map(
	function(element) {
	    return element.label;
	}
    );
    var values = data.map(
	function(element){
	    return element.balance;
	}
    );
    var ctx = document.getElementById("balance-flow");

    var myLineChart = new Chart(ctx, {
	type: 'line',
	data: {
	    labels: labels,
	    datasets: [{
		label: "Balance",
		lineTension: 0.3,
		backgroundColor: "rgba(2,117,216,0.2)",
		borderColor: "rgba(2,117,216,1)",
		pointRadius: 5,
		pointBackgroundColor: "rgba(2,117,216,1)",
		pointBorderColor: "rgba(255,255,255,0.8)",
		pointHoverRadius: 5,
		pointHoverBackgroundColor: "rgba(2,117,216,1)",
		pointHitRadius: 50,
		pointBorderWidth: 2,
		data: values,
	    }],
	},
	options: {
	    scales: {
		xAxes: [{
		    time: {
			unit: 'date'
		    },
		    gridLines: {
			display: false
		    },
		    ticks: {
			maxTicksLimit: 7
		    }
		}],
		yAxes: [{
		    ticks: {
			min: 0,
			max: 20000,
			maxTicksLimit: 5
		    },
		    gridLines: {
			color: "rgba(0, 0, 0, .125)",
		    }
		}],
	    },
	    legend: {
		display: false
	    }
	}
    });
}
