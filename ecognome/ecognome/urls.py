"""ecognome URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import re_path, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    # Admin related URLs
    re_path(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    re_path(r'^admin/', admin.site.urls),
    # Login related URLs
    re_path(r'^accounts/', include([
        re_path(r'^login/$', auth_views.LoginView.as_view(), name='login'),
        re_path(r'^logout/$', auth_views.LogoutView.as_view(), {'next_page': '/'}, name='logout'),
        # Password related URLs
        re_path(r'^password/', include([
            re_path(r'^change/$', auth_views.PasswordChangeView.as_view(), name='password_change'),
            re_path(r'^change/done/$', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
            ])),
        ])),
    # Druids app
    re_path(r'^api/charts/', include('chartsapi.urls', namespace='chartsapi')),
    re_path(r'^', include('druids.urls', namespace='druids')),
]
