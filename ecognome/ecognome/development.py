import os

from .settings import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get("PSQL_DB"),
        'USER': os.environ.get("PSQL_USER"),
        'PASSWORD': os.environ.get("PSQL_PASS"),
        'HOST': os.environ.get("PSQL_HOST"),
        'PORT': os.environ.get("PSQL_PORT"),
    }
}

STATICFILES_DIRS = [
    "static/",
]
