from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal as D

from django.db.models import Sum
from django.db.models.functions import Coalesce
from rest_framework.views import APIView
from rest_framework.response import Response

from druids.models import Entrada
from druids.models import Salida


class BalanceFlowDataView(APIView):
    """
    View for chart visualizing last year balance fluctuation.
    """
    def _build_ending_date(self):
        cutoff = date.today()
        return date(year=cutoff.year, month=cutoff.month, day=1)

    def _get_period_range(self):
        ending_date = self._build_ending_date()
        period_range = [ending_date]
        for i in range(1, 13):
            ending_date -= relativedelta(months=1)
            period_range.append(ending_date)
        return period_range[::-1]

    def _get_balance_for_period(self, period):
        incomes_subtotal = Entrada.objects.filter(
            fecha__lte=period
        ).aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]
        outcomes_subtotal = Salida.objects.filter(
            fecha__lte=period
        ).aggregate(
            subtotal=Coalesce(Sum("importe"), D("0.00")),
        )["subtotal"]
        return incomes_subtotal - outcomes_subtotal

    def _get_balance_fluctuation_list(self):
        period_range = self._get_period_range()
        fluctuation = []
        for period in period_range:
            fluctuation.append(
                {
                    "label": period,
                    "balance": self._get_balance_for_period(period),
                }
            )
        return fluctuation

    def get(self, request, format=None):
        fluctuation = self._get_balance_fluctuation_list()
        return Response(fluctuation)

