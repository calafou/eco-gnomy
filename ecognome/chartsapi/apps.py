from django.apps import AppConfig


class ChartsApiConfig(AppConfig):
    name = "chartsapi"
