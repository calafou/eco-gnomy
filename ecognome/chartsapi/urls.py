from django.urls import path

from . import views

app_name = "chartsapi"

urlpatterns = [
    path(
        'balance-flow/',
        views.BalanceFlowDataView.as_view(),
        name="balance-flow",
    ),
]

