# Eco-gnomy
A Django project to manage Calafou's housing cooperative written in python3.

## Setup development environment

You must have previously installed:

* [docker](https://docs.docker.com/install)
* [docker-compose](https://docs.docker.com/compose/install)

To build and start the development environment for the first time:

```
# run with sudo privileges
$ docker-compose up --build -d

# run with sudo privileges
$ docker-compose exec ecognomy python manage.py migrate

# run with sudo privileges
$ docker-compose exec ecognomy python manage.py createsuperuser
```

To sleep the environment:

```
# run with sudo privileges
$ docker-compose down
```

To awake the environment:

```
# run with sudo privileges
$ docker-compose up -d
```

The development server will be running at:

```
http://localhost:8000
```

## Run tests

To run tests, execute the following once the environment is awake:

```
# run with sudo privileges
$ docker-compose exec ecognomy pytest
```


